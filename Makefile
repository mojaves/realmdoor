all: release

binaries-all: realmdoor realmdoc

binaries-all.exe: realmdoor.exe realmdoc.exe

binaries: realmdoor

binaries.exe: realmdoor.exe

release: release-prepare binaries binaries.exe

realmdoc: release-prepare
	go build -v -o _out/realmdoc cmd/realmdoc/main.go

realmdoor: release-prepare
	go build -v -o _out/realmdoor cmd/realmdoor/main.go

realmdoc.exe: release-prepare
	GOOS=windows GOARCH=amd64 go build -v -o _out/realmdoc.exe cmd/realmdoc/main.go

realmdoor.exe: release-prepare
	GOOS=windows GOARCH=amd64 go build -v -o _out/realmdoor.exe cmd/realmdoor/main.go

bindata:
	go-bindata -pkg assets -o generated/assets/bindata.go assets/...


release-prepare:
	mkdir -p _out

.PHONY: unittest
unittest:
	go test ./pkg/... ./internal/...

.PHONY: unittest-cover
unittest-cover:
	go test -coverprofile=coverage.out ./pkg/... ./internal/...

.PHONY: show-cover
show-cover:
	go tool cover -html=coverage.out

.PHONY: clean
clean:
	rm -rf _out
