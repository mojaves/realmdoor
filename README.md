```bash
 ____  _____    _    _     __  __ ____   ___   ___  ____  
|  _ \| ____|  / \  | |   |  \/  |  _ \ / _ \ / _ \|  _ \ 
| |_) |  _|   / _ \ | |   | |\/| | | | | | | | | | | |_) |
|  _ <| |___ / ___ \| |___| |  | | |_| | |_| | |_| |  _ < 
|_| \_\_____/_/   \_\_____|_|  |_|____/ \___/ \___/|_| \_\
```

# is a tournament manager for tabletop miniature wargaming 

`realmdoor` provides a web ui to manage your tabletop miniature wargame tournament.
`realmdoor` is self-contained, requires no setup and has no additional dependencies.

## license
Apache v2 - **ABSOLUTELY NO WARRANTY**

## how to try it out?

### download pre-built binaries: nightly builds

![Use the download button in the gitlab UI next to the clone button](docs/install/images/download-nightly-build.gif)

### compile it yourself

Just run
```bash
make
```

## screenshots
Coming soon

## roadmap

See [here](ROADMAP.md).

## REST API

See [here](./docs/dev/RESTAPI.md).

## e2e tests

TBD
