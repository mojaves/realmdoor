/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package admin

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	logger "github.com/apsdehal/go-logger"

	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

type Client struct {
	log     *logger.Logger
	baseURL string
}

func New(log *logger.Logger, apiHost string, apiPort int) *Client {
	return &Client{
		log:     log,
		baseURL: fmt.Sprintf("http://%s:%d/api/v1", apiHost, apiPort),
	}
}

func (cli *Client) Players() ([]rdv1.Player, error) {
	apiURL := fmt.Sprintf("%s/player", cli.baseURL)
	resp, err := http.Get(apiURL)
	if err != nil {
		cli.log.Errorf("client: admin: HTTP get error: %s", err)
		return nil, err
	}
	defer resp.Body.Close()
	cli.log.Debugf("client: get: %q -> %d", apiURL, resp.StatusCode)
	if err := errorFromStatusCode(resp.StatusCode); err != nil {
		return nil, err
	}

	var players []rdv1.Player
	err = json.NewDecoder(resp.Body).Decode(&players)
	if err != nil {
		cli.log.Errorf("client: admin: JSON decode error: %s", err)
	}
	return players, err
}

func (cli *Client) Matches() ([]rdv1.Match, error) {
	apiURL := fmt.Sprintf("%s/pairing/round", cli.baseURL)
	resp, err := http.Get(apiURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	cli.log.Debugf("client: get: %q -> %d", apiURL, resp.StatusCode)
	if err := errorFromStatusCode(resp.StatusCode); err != nil {
		return nil, err
	}

	var matches []rdv1.Match
	err = json.NewDecoder(resp.Body).Decode(&matches)
	return matches, err
}

func (cli *Client) MatchResults() ([]rdv1.MatchResult, error) {
	apiURL := fmt.Sprintf("%s/result/round", cli.baseURL)
	resp, err := http.Get(apiURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	cli.log.Debugf("client: get: %q -> %d", apiURL, resp.StatusCode)
	if err := errorFromStatusCode(resp.StatusCode); err != nil {
		return nil, err
	}

	var matchResults []rdv1.MatchResult
	err = json.NewDecoder(resp.Body).Decode(&matchResults)
	return matchResults, err
}

func (cli *Client) Leaderboard() ([]rdv1.Position, error) {
	apiURL := fmt.Sprintf("%s/leaderboard", cli.baseURL)
	resp, err := http.Get(apiURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	cli.log.Debugf("client: get: %q -> %d", apiURL, resp.StatusCode)
	if err := errorFromStatusCode(resp.StatusCode); err != nil {
		return nil, err
	}

	var positions []rdv1.Position
	err = json.NewDecoder(resp.Body).Decode(&positions)
	return positions, err
}

func (cli *Client) CurrentRound() (*rdv1.Round, error) {
	apiURL := fmt.Sprintf("%s/round", cli.baseURL)
	resp, err := http.Get(apiURL)
	if err != nil {
		cli.log.Errorf("client: admin: HTTP get error: %s", err)
		return nil, err
	}
	defer resp.Body.Close()
	cli.log.Debugf("client: get: %q -> %d", apiURL, resp.StatusCode)
	if err := errorFromStatusCode(resp.StatusCode); err != nil {
		return nil, err
	}

	rr := rdv1.Round{}
	err = json.NewDecoder(resp.Body).Decode(&rr)
	if err != nil {
		cli.log.Errorf("client: admin: JSON decode error: %s", err)
	}
	return &rr, err
}

func (cli *Client) AddPlayers(players []rdv1.Player) error {
	apiURL := fmt.Sprintf("%s/player", cli.baseURL)
	return cli.postJSON(apiURL, players)
}

func (cli *Client) AddMatchResults(results []rdv1.MatchResult) error {
	apiURL := fmt.Sprintf("%s/result/match", cli.baseURL)
	return cli.postJSON(apiURL, results)
}

func (cli *Client) AddMatchPlayerResults(results []rdv1.MatchPlayerResult) error {
	apiURL := fmt.Sprintf("%s/result/player", cli.baseURL)
	return cli.postJSON(apiURL, results)
}

func (cli *Client) FirstRound() error {
	apiURL := fmt.Sprintf("%s/round/first", cli.baseURL)
	loc := rdv1.Location{}
	return cli.postJSON(apiURL, loc)
}

func (cli *Client) NextRound(location rdv1.Location) error {
	apiURL := fmt.Sprintf("%s/round/next", cli.baseURL)
	return cli.postJSON(apiURL, location)
}

func (cli *Client) postJSON(apiURL string, payload interface{}) error {
	data, err := json.Marshal(payload)
	if err != nil {
		cli.log.Errorf("client: admin: JSON encode error: %s", err)
		return err
	}
	resp, err := http.Post(apiURL, "application/json", bytes.NewBuffer(data))
	defer resp.Body.Close()
	cli.log.Debugf("client: postJSON: %q -> %d", apiURL, resp.StatusCode)
	if err == nil {
		err = errorFromStatusCode(resp.StatusCode)
	}
	if err != nil {
		cli.log.Errorf("client: admin: HTTP post error: %s", err)
	}
	return err
}

func errorFromStatusCode(statusCode int) error {
	switch statusCode {
	case http.StatusInternalServerError:
		return fmt.Errorf("internal error, server error")
	case http.StatusUnprocessableEntity:
		return fmt.Errorf("internal error, unprocessable request")
	}
	return nil
}
