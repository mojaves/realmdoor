/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package v1_test

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/mojaves/realmdoor/pkg/api/v1"
	"gitlab.com/mojaves/realmdoor/pkg/caosid"
)

func TestMatchIsCompleted(t *testing.T) {
	mr := v1.MatchResult{}
	if mr.IsCompleted() {
		t.Errorf("zero match can't be completed")
	}
}

func TestMatchResultIsCompleted(t *testing.T) {
	mr := v1.MatchResult{}
	if mr.IsCompleted() {
		t.Errorf("zero match result detected completed")
	}

	mr = v1.MatchResult{
		ID:      v1.EID(caosid.Long()),
		MatchID: v1.EID(caosid.Long()),
		Attacker: v1.MatchPlayerResultInfo{
			PlayerID: v1.EID(caosid.Long()),
			Outcome:  v1.OutcomeDraw,
			Score: v1.Score{
				Primary:   100,
				Secondary: 50,
				Tertiary:  100,
			},
		},
		Defender: v1.MatchPlayerResultInfo{
			PlayerID: v1.EID(caosid.Long()),
			Outcome:  v1.OutcomeDraw,
			Score: v1.Score{
				Primary:   100,
				Secondary: 50,
				Tertiary:  100,
			},
		},
	}
	if !mr.IsCompleted() {
		t.Errorf("legit match result misdetected incomplete")
	}
}

func TestMatchResultSplitMerge(t *testing.T) {
	mr := v1.MatchResult{
		ID:      v1.EID(caosid.Long()),
		MatchID: v1.EID(caosid.Long()),
		Attacker: v1.MatchPlayerResultInfo{
			PlayerID: v1.EID(caosid.Long()),
			Outcome:  v1.OutcomeDraw,
			Score: v1.Score{
				Primary:   100,
				Secondary: 50,
				Tertiary:  100,
			},
		},
		Defender: v1.MatchPlayerResultInfo{
			PlayerID: v1.EID(caosid.Long()),
			Outcome:  v1.OutcomeDraw,
			Score: v1.Score{
				Primary:   100,
				Secondary: 50,
				Tertiary:  100,
			},
		},
	}
	mpr := mr.Split()
	if len(mpr) != 2 {
		t.Errorf("unexpected player results, found %d", len(mpr))
	}

	mr2, ok := v1.Merge(mpr[0], mpr[1])
	if mr.ID == mr2.ID {
		t.Errorf("merged match result got back initial ID!!!")
	}
	mr2.ID = mr.ID // hack to make the DeepEqual happy
	if !ok || !reflect.DeepEqual(mr, mr2) {
		t.Errorf("split/merge failed, ok=%v mpr=%#v mr=%#v mr2=%#v", ok, mpr, mr, mr2)
	}
}

func TestMatchResultMergeMismatch(t *testing.T) {
	defender := v1.MatchPlayerResult{
		ID:      v1.EID(caosid.Long()),
		MatchID: v1.EID(caosid.Long()),
		Result: v1.MatchPlayerResultInfo{
			PlayerID: v1.EID(caosid.Long()),
			Outcome:  v1.OutcomeDraw,
			Score: v1.Score{
				Primary:   100,
				Secondary: 50,
				Tertiary:  100,
			},
		},
	}
	attacker := v1.MatchPlayerResult{
		ID:      v1.EID(caosid.Long()),
		MatchID: v1.EID(caosid.Long()),
		Result: v1.MatchPlayerResultInfo{
			PlayerID: v1.EID(caosid.Long()),
			Outcome:  v1.OutcomeDraw,
			Score: v1.Score{
				Primary:   100,
				Secondary: 50,
				Tertiary:  100,
			},
		},
	}
	if _, ok := v1.Merge(defender, attacker); ok {
		t.Errorf("Merge succeded with result referring to different matches - and it shouldn't")
	}
}

func TestMatchResultValidate(t *testing.T) {
	m := v1.Match{
		ID:         v1.EID(caosid.Long()),
		Round:      1,
		AttackerID: v1.EID(caosid.Long()),
		DefenderID: v1.EID(caosid.Long()),
	}
	tcases := []struct {
		name    string
		isValid bool
		mr      v1.MatchResult
	}{
		{
			"draw identical",
			true,
			v1.MatchResult{
				ID:      v1.EID(caosid.Long()),
				MatchID: m.ID,
				Attacker: v1.MatchPlayerResultInfo{
					PlayerID: m.AttackerID,
					Outcome:  v1.OutcomeDraw,
					Score: v1.Score{
						Primary:   100,
						Secondary: 50,
						Tertiary:  100,
					},
				},
				Defender: v1.MatchPlayerResultInfo{
					PlayerID: m.DefenderID,
					Outcome:  v1.OutcomeDraw,
					Score: v1.Score{
						Primary:   100,
						Secondary: 50,
						Tertiary:  100,
					},
				},
			},
		},
		{
			"defender win",
			true,
			v1.MatchResult{
				ID:      v1.EID(caosid.Long()),
				MatchID: m.ID,
				Attacker: v1.MatchPlayerResultInfo{
					PlayerID: m.AttackerID,
					Outcome:  v1.OutcomeMajorLoss,
					Score: v1.Score{
						Primary:   20,
						Secondary: 50,
						Tertiary:  100,
					},
				},
				Defender: v1.MatchPlayerResultInfo{
					PlayerID: m.DefenderID,
					Outcome:  v1.OutcomeMajorWin,
					Score: v1.Score{
						Primary:   60,
						Secondary: 30,
						Tertiary:  80,
					},
				},
			},
		},
		{
			"attacker win",
			true,
			v1.MatchResult{
				ID:      v1.EID(caosid.Long()),
				MatchID: m.ID,
				Attacker: v1.MatchPlayerResultInfo{
					PlayerID: m.AttackerID,
					Outcome:  v1.OutcomeMajorWin,
					Score: v1.Score{
						Primary:   60,
						Secondary: 200,
						Tertiary:  100,
					},
				},
				Defender: v1.MatchPlayerResultInfo{
					PlayerID: m.DefenderID,
					Outcome:  v1.OutcomeMajorLoss,
					Score: v1.Score{
						Primary:   0,
						Secondary: 400,
						Tertiary:  500,
					},
				},
			},
		},
		{
			"draw matchid mismatch",
			false,
			v1.MatchResult{
				ID:      v1.EID(caosid.Long()),
				MatchID: v1.EID(caosid.Long()),
				Attacker: v1.MatchPlayerResultInfo{
					PlayerID: m.AttackerID,
					Outcome:  v1.OutcomeDraw,
					Score: v1.Score{
						Primary:   100,
						Secondary: 50,
						Tertiary:  100,
					},
				},
				Defender: v1.MatchPlayerResultInfo{
					PlayerID: m.DefenderID,
					Outcome:  v1.OutcomeDraw,
					Score: v1.Score{
						Primary:   100,
						Secondary: 50,
						Tertiary:  100,
					},
				},
			},
		},
		{
			"defender win but outcome mismatch",
			false,
			v1.MatchResult{
				ID:      v1.EID(caosid.Long()),
				MatchID: m.ID,
				Attacker: v1.MatchPlayerResultInfo{
					PlayerID: m.AttackerID,
					Outcome:  v1.OutcomeMajorLoss,
					Score: v1.Score{
						Primary:   20,
						Secondary: 50,
						Tertiary:  100,
					},
				},
				Defender: v1.MatchPlayerResultInfo{
					PlayerID: m.DefenderID,
					Outcome:  v1.OutcomeMinorWin,
					Score: v1.Score{
						Primary:   60,
						Secondary: 30,
						Tertiary:  80,
					},
				},
			},
		},
		{
			"draw score mismatch",
			false,
			v1.MatchResult{
				ID:      v1.EID(caosid.Long()),
				MatchID: m.ID,
				Attacker: v1.MatchPlayerResultInfo{
					PlayerID: m.AttackerID,
					Outcome:  v1.OutcomeDraw,
					Score: v1.Score{
						Primary:   100,
						Secondary: 50,
						Tertiary:  90,
					},
				},
				Defender: v1.MatchPlayerResultInfo{
					PlayerID: m.DefenderID,
					Outcome:  v1.OutcomeDraw,
					Score: v1.Score{
						Primary:   100,
						Secondary: 50,
						Tertiary:  100,
					},
				},
			},
		},
		{
			"defender score mismatch",
			false,
			v1.MatchResult{
				ID:      v1.EID(caosid.Long()),
				MatchID: m.ID,
				Attacker: v1.MatchPlayerResultInfo{
					PlayerID: m.AttackerID,
					Outcome:  v1.OutcomeMajorWin,
					Score: v1.Score{
						Primary:   20,
						Secondary: 50,
						Tertiary:  100,
					},
				},
				Defender: v1.MatchPlayerResultInfo{
					PlayerID: m.DefenderID,
					Outcome:  v1.OutcomeMajorLoss,
					Score: v1.Score{
						Primary:   60,
						Secondary: 30,
						Tertiary:  80,
					},
				},
			},
		},
		{
			"attacker score mismatch",
			false,
			v1.MatchResult{
				ID:      v1.EID(caosid.Long()),
				MatchID: m.ID,
				Attacker: v1.MatchPlayerResultInfo{
					PlayerID: m.AttackerID,
					Outcome:  v1.OutcomeMajorLoss,
					Score: v1.Score{
						Primary:   60,
						Secondary: 200,
						Tertiary:  100,
					},
				},
				Defender: v1.MatchPlayerResultInfo{
					PlayerID: m.DefenderID,
					Outcome:  v1.OutcomeMajorWin,
					Score: v1.Score{
						Primary:   0,
						Secondary: 400,
						Tertiary:  500,
					},
				},
			},
		},
	}
	for _, tcase := range tcases {
		t.Run(tcase.name, func(t *testing.T) {
			err := tcase.mr.Validate(m)
			isValid := (err == nil)
			if isValid != tcase.isValid {
				t.Errorf("%s validate expected=%v got=%v err=%v", tcase.name, tcase.isValid, isValid, err)
			}
		})
	}
}

func TestLeaderboardBaseMethods(t *testing.T) {
	players := []v1.EID{
		v1.EID("AAA"),
		v1.EID("BBB"),
		v1.EID("CCC"),
		v1.EID("DDD"),
		v1.EID("EEE"),
		v1.EID("FFF"),
	}
	lb := v1.NewLeaderboardForPlayers(players)
	if lb.Len() != len(players) {
		t.Errorf("leaderboard length mismatch expected %d got %d", len(players), lb.Len())
	}
	playersBack := lb.Players()
	if !reflect.DeepEqual(players, playersBack) {
		t.Errorf("leaderboard players mismatch expected %v got %v", players, playersBack)
	}

	playersReversed := []v1.EID{
		v1.EID("FFF"),
		v1.EID("EEE"),
		v1.EID("DDD"),
		v1.EID("CCC"),
		v1.EID("BBB"),
		v1.EID("AAA"),
	}
	lb2 := lb.Reversed()
	playersReversedBack := lb2.Players()
	if !reflect.DeepEqual(playersReversed, playersReversedBack) {
		t.Errorf("leaderboard players mismatch expected %v got %v", playersReversed, playersReversedBack)
	}

	lbS := lb.Sorted()
	playersSortedBack := lbS.Players()
	if !reflect.DeepEqual(players, playersSortedBack) {
		t.Errorf("leaderboard players already sorted expected %v got %v", players, playersSortedBack)
	}
}

func TestLeaderboardPlayersAt(t *testing.T) {
	players := []v1.EID{
		v1.EID("AAA"),
		v1.EID("BBB"),
		v1.EID("CCC"),
		v1.EID("DDD"),
		v1.EID("EEE"),
		v1.EID("FFF"),
	}
	lb := v1.NewLeaderboardForPlayers(players)

	tcases := []struct {
		pos      int
		expected v1.EID
	}{
		{
			pos:      -3,
			expected: "",
		},
		{
			pos:      42,
			expected: "",
		},
		{
			pos:      0,
			expected: v1.EID("AAA"),
		},
	}

	for _, tcase := range tcases {
		t.Run(fmt.Sprintf("@%d -> %q", tcase.pos, string(tcase.expected)), func(t *testing.T) {
			got := lb.PlayerAt(tcase.pos)
			if got != tcase.expected {
				t.Errorf("player@%d got=%q expected=%q", tcase.pos, string(got), string(tcase.expected))
			}
		})
	}
}

func TestLeaderboardUpdate(t *testing.T) {
	scores := map[v1.EID]v1.Score{
		v1.EID("AAA"): v1.Score{
			Primary:   0,
			Secondary: 0,
			Tertiary:  1500,
		},
		v1.EID("BBB"): v1.Score{
			Primary:   0,
			Secondary: 30,
			Tertiary:  1000,
		},
		v1.EID("CCC"): v1.Score{
			Primary:   60,
			Secondary: 30,
			Tertiary:  1000,
		},
		v1.EID("DDD"): v1.Score{
			Primary:   60,
			Secondary: 30,
			Tertiary:  1200,
		},
		v1.EID("EEE"): v1.Score{
			Primary:   60,
			Secondary: 20,
			Tertiary:  400,
		},
		v1.EID("FFF"): v1.Score{
			Primary:   0,
			Secondary: 20,
			Tertiary:  500,
		},
	}

	players := []v1.EID{
		v1.EID("AAA"),
		v1.EID("BBB"),
		v1.EID("CCC"),
		v1.EID("DDD"),
		v1.EID("EEE"),
		v1.EID("FFF"),
	}

	playersExpected := []v1.EID{
		v1.EID("DDD"),
		v1.EID("CCC"),
		v1.EID("EEE"),
		v1.EID("BBB"),
		v1.EID("FFF"),
		v1.EID("AAA"),
	}

	lb := v1.NewLeaderboardForPlayers(players)
	lb2, err := lb.Update(func(playerID v1.EID) (v1.Score, error) {
		return scores[playerID], nil
	})
	if err != nil {
		t.Errorf("unexpected update error: %v", err)
	}
	playersBack := lb2.Players()
	if !reflect.DeepEqual(playersExpected, playersBack) {
		t.Errorf("leaderboard players mismatch after updated expected %v got %v", playersExpected, playersBack)
	}
}
