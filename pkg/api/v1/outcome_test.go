/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package v1_test

import (
	"fmt"
	"testing"

	"gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

func TestOutcomeOpposite(t *testing.T) {
	tcases := []struct {
		oc v1.Outcome
		op v1.Outcome
	}{
		{
			oc: v1.OutcomeMajorWin,
			op: v1.OutcomeMajorLoss,
		},
		{
			oc: v1.OutcomeMinorWin,
			op: v1.OutcomeMinorLoss,
		},
		{
			oc: v1.OutcomeDraw,
			op: v1.OutcomeDraw,
		},
		{
			oc: v1.OutcomeMinorLoss,
			op: v1.OutcomeMinorWin,
		},
		{
			oc: v1.OutcomeMajorLoss,
			op: v1.OutcomeMajorWin,
		},
	}
	for _, tcase := range tcases {
		t.Run(fmt.Sprintf("%s vs %s", tcase.op.String(), tcase.oc.String()), func(t *testing.T) {
			if !tcase.op.IsOppositeOutcome(tcase.oc) {
				t.Errorf("%s is not opposite of %s", tcase.op.String(), tcase.oc.String())
			}
			op, _ := tcase.oc.Opposite()
			if op != tcase.op {
				t.Errorf("%s opposite got %s expected %v", tcase.oc.String(), op.String(), tcase.op.String())
			}
		})
	}
}

func TestOutcomeValid(t *testing.T) {
	tcases := []struct {
		oc    v1.Outcome
		valid bool
	}{
		{
			oc:    v1.OutcomeMajorWin,
			valid: true,
		},
		{
			oc:    v1.OutcomeMinorWin,
			valid: true,
		},
		{
			oc:    v1.OutcomeDraw,
			valid: true,
		},
		{
			oc:    v1.OutcomeMinorLoss,
			valid: true,
		},
		{
			oc:    v1.OutcomeMajorLoss,
			valid: true,
		},
		{
			oc:    v1.Outcome(-5),
			valid: false,
		},
		{
			oc:    v1.Outcome(11),
			valid: false,
		},
	}
	for _, tcase := range tcases {
		t.Run(fmt.Sprintf("%s valid=%v", tcase.oc.String(), tcase.valid), func(t *testing.T) {
			ret := tcase.oc.IsValid()
			if ret != tcase.valid {
				t.Errorf("%s valid=%v expected=%v", tcase.oc.String(), ret, tcase.valid)
			}
		})
	}
}

func TestOutcomeIsWinLoss(t *testing.T) {
	tcases := []struct {
		oc     v1.Outcome
		isWin  bool
		isLoss bool
	}{
		{
			oc:     v1.OutcomeMajorWin,
			isWin:  true,
			isLoss: false,
		},
		{
			oc:     v1.OutcomeMinorWin,
			isWin:  true,
			isLoss: false,
		},
		{
			oc:     v1.OutcomeDraw,
			isWin:  false,
			isLoss: false,
		},
		{
			oc:     v1.OutcomeMinorLoss,
			isWin:  false,
			isLoss: true,
		},
		{
			oc:     v1.OutcomeMajorLoss,
			isWin:  false,
			isLoss: true,
		},
	}
	for _, tcase := range tcases {
		t.Run(fmt.Sprintf("%s win=%v loss=%v", tcase.oc.String(), tcase.isWin, tcase.isLoss), func(t *testing.T) {
			W := tcase.oc.IsWin()
			L := tcase.oc.IsLoss()
			if W != tcase.isWin {
				t.Errorf("%s expected win=%v got win=%v", tcase.oc.String(), tcase.isWin, W)
			}
			if L != tcase.isLoss {
				t.Errorf("%s expected loss=%v got loss=%v", tcase.oc.String(), tcase.isLoss, L)
			}
		})
	}
}
