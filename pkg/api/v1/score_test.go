/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */
package v1_test

import (
	"reflect"
	"testing"

	"gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

func TestScoreToSlice(t *testing.T) {
	s := v1.Score{
		Primary:   42,
		Secondary: 13,
		Tertiary:  81,
	}
	got := s.ToSlice()
	expected := []int{
		81, 13, 42,
	}
	if !reflect.DeepEqual(got, expected) {
		t.Errorf("got %#v expected %#v", got, expected)
	}
}

func TestScoreToUint64(t *testing.T) {
	s := v1.Score{
		Primary:   42,
		Secondary: 13,
		Tertiary:  81,
	}
	got := s.ToUint64()
	expected := uint64(42013081)
	if got != expected {
		t.Errorf("got %v expected %v", got, expected)
	}
}

func TestScoreCompare(t *testing.T) {
	tcases := []struct {
		name   string
		s1     v1.Score
		s2     v1.Score
		isLess bool
	}{
		{
			"diff by secs",
			v1.Score{
				Primary:   60,
				Secondary: 0,
				Tertiary:  1800,
			},
			v1.Score{
				Primary:   0,
				Secondary: 30,
				Tertiary:  800,
			},
			false,
		},
	}
	for _, tcase := range tcases {
		t.Run(tcase.name, func(t *testing.T) {
			res := tcase.s1.Less(tcase.s2)
			if res != tcase.isLess {
				t.Errorf("%s < %s expected %v got %v", tcase.s1, tcase.s2, tcase.isLess, res)
			}
		})
	}
}
