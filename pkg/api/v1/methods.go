/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package v1

import (
	"fmt"
	"sort"

	"gitlab.com/mojaves/realmdoor/pkg/caosid"
)

func (loc Location) IsValid() bool {
	return loc.Realm != "" && loc.Scenario != ""
}

func (loc Location) String() string {
	return fmt.Sprintf("%s on %s", loc.Scenario, loc.Realm)
}

func (p Player) String() string {
	return fmt.Sprintf("%s %s", p.Firstname, p.Lastname)
}

func (m MatchResult) IsCompleted() bool {
	return m.Defender.PlayerID != "" && m.Attacker.PlayerID != ""
}

func (m MatchResult) Split() []MatchPlayerResult {
	return []MatchPlayerResult{
		MatchPlayerResult{
			ID:      EID(caosid.Long()),
			MatchID: m.MatchID,
			Result:  m.Defender,
		},
		MatchPlayerResult{
			ID:      EID(caosid.Long()),
			MatchID: m.MatchID,
			Result:  m.Attacker,
		},
	}
}

func Merge(defender, attacker MatchPlayerResult) (MatchResult, bool) {
	if defender.MatchID != attacker.MatchID {
		return MatchResult{}, false
	}
	mr := MatchResult{
		ID:       EID(caosid.Long()),
		MatchID:  defender.MatchID,
		Attacker: attacker.Result,
		Defender: defender.Result,
	}
	if err := mr.validateResult(); err != nil {
		return MatchResult{}, false
	}
	return mr, true
}

func (mr *MatchResult) Validate(match Match) error {
	if mr.MatchID != match.ID {
		return fmt.Errorf("result %q doesn't belong to match %q", mr.MatchID, match.ID)
	}
	return mr.validateResult()
}

func (mr *MatchResult) validateResult() error {
	if !mr.Defender.Outcome.IsOppositeOutcome(mr.Attacker.Outcome) {
		return fmt.Errorf("result mismatch match %s: %q with %s VS %q with %s", mr.ID, mr.Defender.PlayerID, mr.Defender.Outcome, mr.Attacker.PlayerID, mr.Attacker.Outcome)
	}
	if mr.Defender.Outcome == OutcomeDraw {
		if !mr.Defender.Score.Equal(mr.Attacker.Score) {
			return fmt.Errorf("scoring error in %q: Defender draw with different score than Attacker", mr.ID)
		}
	} else if mr.Defender.Outcome.IsWin() {
		if mr.Defender.Score.Less(mr.Attacker.Score) {
			return fmt.Errorf("scoring error in %q: Defender won with less score than Attacker", mr.ID)
		}
	} else { // Defender loss
		if mr.Attacker.Score.Less(mr.Defender.Score) {
			return fmt.Errorf("scoring error in %q: Attacker won with less score than Defender", mr.ID)
		}
	}
	return nil
}

func (pos Position) Less(op Position) bool {
	return pos.Points.Less(op.Points)
}

func (pos Position) Add(s Score) Position {
	res := Position{
		PlayerID:   pos.PlayerID,
		MatchCount: pos.MatchCount + 1,
		Points: Score{
			Primary:   pos.Points.Primary + s.Primary,
			Secondary: pos.Points.Secondary + s.Secondary,
			Tertiary:  pos.Points.Tertiary + s.Tertiary,
		},
	}
	return res
}

func NewLeaderboardForPlayers(playerIDs []EID) Leaderboard {
	lb := Leaderboard{
		Positions: make([]Position, len(playerIDs), len(playerIDs)),
	}
	for j, playerID := range playerIDs {
		lb.Positions[j].PlayerID = playerID
	}
	return lb
}

func (lb Leaderboard) Players() []EID {
	playerIDs := make([]EID, lb.Len())
	for idx, pos := range lb.Positions {
		playerIDs[idx] = pos.PlayerID
	}
	return playerIDs
}

func (lb Leaderboard) Update(getScore func(playerID EID) (Score, error)) (Leaderboard, error) {
	res := Leaderboard{
		Positions: make([]Position, lb.Len(), lb.Len()),
	}
	for i, pos := range lb.Positions {
		score, err := getScore(pos.PlayerID)
		if err != nil {
			return res, err
		}
		res.Positions[i] = pos.Add(score)
		res.Positions[i].MatchCount++
	}
	return res.Sorted(), nil
}

func (lb Leaderboard) PlayerAt(idx int) EID {
	if idx < 0 || idx > lb.Len() {
		return EID("")
	}
	return lb.Positions[idx].PlayerID
}

func (lb Leaderboard) Len() int {
	return len(lb.Positions)
}

func (lb Leaderboard) Less(i, j int) bool {
	return lb.Positions[i].Less(lb.Positions[j])
}

func (lb Leaderboard) Swap(i, j int) {
	lb.Positions[i], lb.Positions[j] = lb.Positions[j], lb.Positions[i]
}

func (lb Leaderboard) ToSlice() []Position {
	res := make([]Position, len(lb.Positions))
	copy(res, lb.Positions)
	return res
}

func (lb Leaderboard) Reversed() Leaderboard {
	res := Leaderboard{
		Positions: make([]Position, len(lb.Positions), len(lb.Positions)),
	}
	for i, j := 0, res.Len()-1; j >= 0; i, j = i+1, j-1 {
		res.Positions[i] = lb.Positions[j]
	}
	return res
}

func (lb Leaderboard) Sorted() Leaderboard {
	if sort.IsSorted(lb) {
		return lb
	}
	sort.Stable(lb)
	return lb.Reversed()
}

func (r *Round) IsCompleted() bool {
	return r.CompletedMatches == r.TotalMatches
}
