/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package v1

type EID string

type Player struct {
	ID        EID    `json:"id"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Faction   string `json:"faction"`
}

// Outcome is always from the defender perspective
type Match struct {
	ID         EID `json:"id"`
	LocationID EID `json:"location_id"`
	Round      int `json:"round"`
	AttackerID EID `json:"attacker_player_id"`
	DefenderID EID `json:"defender_player_id"`
}

type Outcome int

const (
	OutcomeMajorLoss = -2
	OutcomeMinorLoss = -1
	OutcomeDraw      = +0
	OutcomeMinorWin  = +1
	OutcomeMajorWin  = +2
)

type Score struct {
	Primary   int `json:"primary"`
	Secondary int `json:"secondary"`
	Tertiary  int `json:"tertiary"`
}

type Position struct {
	PlayerID   EID   `json:"player_id"`
	MatchCount int   `json:"matches"`
	Points     Score `json:"score"`
}

type Leaderboard struct {
	Positions []Position `json:"data"`
}

type RealmInfo struct {
	Realms    []string `json:"realms"`
	Scenarios []string `json:"scenarios"`
}

type Location struct {
	ID       EID    `json:"id"`
	Realm    string `json:"realm"`
	Scenario string `json:"scenario"`
}

type Round struct {
	Number           int `json:"number"`
	CompletedMatches int `json:"completed_matches"`
	TotalMatches     int `json:"total_matches"`
}

// MatchPlayerResultInfo is a utility type, never used directly
type MatchPlayerResultInfo struct {
	PlayerID EID     `json:"player_id"`
	Outcome  Outcome `json:"outcome"`
	Score    Score   `json:"score"`
}

type MatchPlayerResult struct {
	ID      EID                   `json:"id"`
	MatchID EID                   `json:"match_id"`
	Result  MatchPlayerResultInfo `json:"result"`
}

type MatchResult struct {
	ID       EID                   `json:"id"`
	MatchID  EID                   `json:"match_id"`
	Attacker MatchPlayerResultInfo `json:"attacker_result"`
	Defender MatchPlayerResultInfo `json:"defender_result"`
}

type MatchPlayerConfirmation struct {
	ID       EID `json:"id"`
	MatchID  EID `json:"match_id"`
	PlayerID EID `json:"player_id"`
	// TODO: cookie
}
