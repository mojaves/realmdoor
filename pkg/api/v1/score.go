/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package v1

import (
	"fmt"
)

func (s Score) String() string {
	return fmt.Sprintf("%d,%d,%d", s.Primary, s.Secondary, s.Tertiary)
}

func (s Score) Equal(as Score) bool {
	return s.Primary == as.Primary && s.Secondary == as.Secondary && s.Tertiary == as.Tertiary
}

func (s Score) Less(as Score) bool {
	return s.ToUint64() < as.ToUint64()
}

func (s Score) ToSlice() []int {
	return []int{
		s.Tertiary, s.Secondary, s.Primary,
	}
}

func (s Score) ToUint64() uint64 {
	return uint64(s.Primary*1000*1000 + s.Secondary*1000 + s.Tertiary)
}
