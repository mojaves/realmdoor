/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package v1

func (oc Outcome) IsOppositeOutcome(o Outcome) bool {
	o2, _ := oc.Opposite()
	return o2 == o
}

func (oc Outcome) IsValid() bool {
	_, ok := oc.Opposite()
	return ok
}

func (oc Outcome) IsWin() bool {
	return oc == OutcomeMajorWin || oc == OutcomeMinorWin
}

func (oc Outcome) IsLoss() bool {
	return oc == OutcomeMajorLoss || oc == OutcomeMinorLoss
}

func (oc Outcome) Opposite() (Outcome, bool) {
	switch oc {
	case OutcomeMajorLoss:
		return OutcomeMajorWin, true
	case OutcomeMinorLoss:
		return OutcomeMinorWin, true
	case OutcomeDraw:
		return OutcomeDraw, true
	case OutcomeMinorWin:
		return OutcomeMinorLoss, true
	case OutcomeMajorWin:
		return OutcomeMajorLoss, true
	}
	return OutcomeDraw, false
}

func (oc Outcome) String() string {
	switch oc {
	case OutcomeMajorLoss:
		return "Major Loss"
	case OutcomeMinorLoss:
		return "Minor Loss"
	case OutcomeDraw:
		return "Draw"
	case OutcomeMinorWin:
		return "Minor Victory"
	case OutcomeMajorWin:
		return "Major Victory"
	}
	return "Unknown"
}
