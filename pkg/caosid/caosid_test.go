/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package caosid_test

import (
	"testing"

	"gitlab.com/mojaves/realmdoor/pkg/caosid"
)

func TestCaosIDShort(t *testing.T) {
	runs := 9999
	ids := make(map[string]bool)
	for i := 0; i < runs; i++ {
		id := caosid.Short()
		if _, ok := ids[id]; ok {
			t.Errorf("duplicated short id %q in run %d/%d", id, i, runs)
		}
	}
}

func TestCaosIDLong(t *testing.T) {
	runs := 999999
	ids := make(map[string]bool)
	for i := 0; i < runs; i++ {
		id := caosid.Long()
		if _, ok := ids[id]; ok {
			t.Errorf("duplicated long id %q in run %d/%d", id, i, runs)
		}
	}
}
