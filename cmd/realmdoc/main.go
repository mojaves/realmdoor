package main

import (
	"encoding/json"
	"fmt"
	"os"
	"text/tabwriter"

	logger "github.com/apsdehal/go-logger"

	"gitlab.com/mojaves/realmdoor/internal/pkg/datarepo"
	"gitlab.com/mojaves/realmdoor/internal/pkg/realmdoor"
	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

func printEndpoints(rd *realmdoor.Realmdoor) {
	// func NewWriter(output io.Writer, minwidth, tabwidth, padding int, padchar byte, flags uint) *Writer {
	w := tabwriter.NewWriter(os.Stdout, 8, 4, 0, ' ', tabwriter.FilterHTML)
	for _, route := range rd.Routes() {
		fmt.Fprintf(w, "%s\t%s\n", route.Method, route.Pattern)
	}
	w.Flush()
}

func printEndpointParams(rd *realmdoor.Realmdoor, method, pattern string) {
	param, ok := rd.RouteParamsByPattern(method, pattern)
	if !ok {
		fmt.Fprintf(os.Stderr, "unknown route %s method %s\n", pattern, method)
		return
	}
	fmt.Fprintf(os.Stdout, "# Input:\n")
	if err := json.NewEncoder(os.Stdout).Encode(param.Input); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
	}
	fmt.Fprintf(os.Stdout, "# Output:\n")
	if err := json.NewEncoder(os.Stdout).Encode(param.Output); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
	}
}

func main() {
	log, _ := logger.New("realmdoor", 0, logger.CriticalLevel)
	repo := datarepo.RepoNull{}
	rd := realmdoor.New(log, repo, rdv1.RealmInfo{})

	if len(os.Args) == 3 {
		printEndpointParams(rd, os.Args[1], os.Args[2])
	} else {
		printEndpoints(rd)
	}
}
