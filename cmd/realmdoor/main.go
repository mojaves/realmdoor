/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"

	logger "github.com/apsdehal/go-logger"
	"github.com/spf13/pflag"

	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"

	"gitlab.com/mojaves/realmdoor/internal/pkg/adminui"
	"gitlab.com/mojaves/realmdoor/internal/pkg/datarepo"
	"gitlab.com/mojaves/realmdoor/internal/pkg/realmdoor"
)

type Options struct {
	APIPort     int
	APIHost     string
	AdminPort   int
	AdminHost   string
	Verbose     int
	LogFilePath string
	RandomSeed  int64
}

func (o Options) APIAddress() string {
	return fmt.Sprintf("%s:%d", o.APIHost, o.APIPort)
}

func (o Options) AdminAddress() string {
	return fmt.Sprintf("%s:%d", o.AdminHost, o.AdminPort)
}

func main() {
	var opts Options

	pflag.IntVar(&opts.APIPort, "api-port", 42000, "listening port")
	pflag.StringVar(&opts.APIHost, "api-host", "localhost", "listening address/host")
	pflag.IntVar(&opts.AdminPort, "admin-port", 43000, "listening port")
	pflag.StringVar(&opts.AdminHost, "admin-host", "localhost", "listening address/host")
	pflag.IntVarP(&opts.Verbose, "verbose", "v", 1, "verbosiness level")
	pflag.StringVarP(&opts.LogFilePath, "log-file", "L", "realmdoor.jlog", "log file path")
	pflag.Int64VarP(&opts.RandomSeed, "randomseed", "r", 0, "random seed to use - 0 means autogenerate")
	pflag.Parse()

	log, _ := logger.New("realmdoor", 1, logger.DebugLevel)

	if opts.RandomSeed == 0 {
		opts.RandomSeed = time.Now().UnixNano()
	}
	rand.Seed(opts.RandomSeed)

	var err error
	var repo datarepo.DataRepo
	if opts.LogFilePath != "" {
		repo, err = datarepo.NewRepoJLog(log, opts.LogFilePath)
		if err != nil {
			log.Fatalf("repo jlog error: %v", err)
		}

	} else {
		repo = datarepo.RepoNull{}
	}

	rd := realmdoor.New(log, repo, rdv1.RealmInfo{})
	rd.Replay() // failures not critical

	apiAddr := opts.APIAddress()
	adminAddr := opts.AdminAddress()

	go func() {
		aui := adminui.New(log, opts.APIHost, opts.APIPort)
		log.Noticef("realmdoor Admin starting, listening on '%s', connecting to '%s'", adminAddr, apiAddr)
		log.Fatalf("%s", http.ListenAndServe(adminAddr, aui.NewRouter()))
	}()

	log.Noticef("realmdoor API starting, listening on '%s'", apiAddr)
	log.Fatalf("%s", http.ListenAndServe(apiAddr, rd.NewRouter()))
}
