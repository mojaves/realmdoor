/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package main

import (
	"fmt"
	"net/http"

	logger "github.com/apsdehal/go-logger"
	"github.com/spf13/pflag"

	"gitlab.com/mojaves/realmdoor/internal/pkg/adminui"
)

type Options struct {
	APIPort   int
	APIHost   string
	AdminPort int
	AdminHost string
}

func (o Options) APIAddress() string {
	return fmt.Sprintf("%s:%d", o.APIHost, o.APIPort)
}

func (o Options) AdminAddress() string {
	return fmt.Sprintf("%s:%d", o.AdminHost, o.AdminPort)
}

func main() {
	var opts Options

	pflag.IntVar(&opts.APIPort, "api-port", 42000, "listening port")
	pflag.StringVar(&opts.APIHost, "api-host", "localhost", "listening address/host")
	pflag.IntVar(&opts.AdminPort, "admin-port", 43000, "listening port")
	pflag.StringVar(&opts.AdminHost, "admin-host", "localhost", "listening address/host")
	pflag.Parse()

	log, _ := logger.New("realmdoor", 1, logger.DebugLevel)

	apiAddr := opts.APIAddress()
	adminAddr := opts.AdminAddress()

	aui := adminui.New(log, opts.APIHost, opts.APIPort)
	log.Noticef("realmdoor Admin starting, listening on '%s', connecting to '%s'", adminAddr, apiAddr)
	log.Fatalf("%s", http.ListenAndServe(adminAddr, aui.NewRouter()))
}
