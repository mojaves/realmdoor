# Realmdoor rest API


check the `realmdoc` tool. It can be run in two modes.

If you just hit `realmdoc`, it will emit all the routes supported by the corresponding version of `realmdoor.
Example:
```bash
$ realmdoc 
GET     /api/v1/leaderboard
GET     /api/v1/round
GET     /api/v1/player
GET     /api/v1/player/{playerId}
GET     /api/v1/pairing/round
GET     /api/v1/pairing/round/{roundNum}
GET     /api/v1/result/round
GET     /api/v1/result/round/{roundNum}
GET     /api/v1/result/{matchId}
POST    /api/v1/player
POST    /api/v1/result
POST    /api/v1/round/next
POST    /api/v1/round/first
```

If you feed `realmdoc` with the METHOD and the PATTERN argument, it will output the expected input/output JSON types.
Example:

```
realmdoc GET '/api/v1/player'
# Input:
[{"id":"","name":"","surname":"","faction":""}]
# Output:
null

realmdoc POST '/api/v1/player'
# Input:
null
# Output:
[{"id":"","name":"","surname":"","faction":""}]

realmdoc POST '/api/v1/result'
# Input:
null
# Output:
[{"id":"","match_id":"","player_id":"","outcome":0,"score":{"primary":0,"secondary":0,"tertiary":0}}]
```
