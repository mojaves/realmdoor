package datarepo

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"os"

	logger "github.com/apsdehal/go-logger"

	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

const (
	separator string = "---"
)

type RepoJLog struct {
	log     *logger.Logger
	sink    *os.File
	entries map[string]map[int]interface{}
}

func NewRepoJLog(log *logger.Logger, path string) (*RepoJLog, error) {
	entries, err := replay(log, path)
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		return nil, err
	}

	fh, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return nil, err
	}
	return &RepoJLog{
		log:     log,
		sink:    fh,
		entries: entries,
	}, nil
}

func (rj *RepoJLog) CountRounds() (int, error) {
	return 0, fmt.Errorf("not implemented")
}

const (
	KindPlayers  = "players"
	KindPairings = "pairings"
	KindResults  = "results"
	KindLocation = "location"
)

type genericEntry struct {
	Kind  string `json:"kind"`
	Round int    `json:"round"`
}

type playersEntry struct {
	Kind  string        `json:"kind"`
	Round int           `json:"round"`
	Data  []rdv1.Player `json:"data"`
}

type pairingsEntry struct {
	Kind  string       `json:"kind"`
	Round int          `json:"round"`
	Data  []rdv1.Match `json:"data"`
}

type resultsEntry struct {
	Kind  string             `json:"kind"`
	Round int                `json:"round"`
	Data  []rdv1.MatchResult `json:"data"`
}

type locationEntry struct {
	Kind  string        `json:"kind"`
	Round int           `json:"round"`
	Data  rdv1.Location `json:"data"`
}

func replay(log *logger.Logger, path string) (map[string]map[int]interface{}, error) {
	entries := map[string]map[int]interface{}{
		KindPlayers:  make(map[int]interface{}),
		KindPairings: make(map[int]interface{}),
		KindResults:  make(map[int]interface{}),
		KindLocation: make(map[int]interface{}),
	}

	var err error
	src, err := os.Open(path)
	if err != nil {
		return entries, err
	}
	defer src.Close()

	lines := 1
	entities := 0
	scanner := bufio.NewScanner(src)
	var buf bytes.Buffer
	for scanner.Scan() {
		lines++

		data := scanner.Text()
		if data != separator {
			// accumulate
			buf.WriteString(data)
			continue
		}
		// silently drop separator
		entryRaw := buf.Bytes()
		var ge genericEntry
		err = json.Unmarshal(entryRaw, &ge)
		if err != nil {
			log.Warningf("entity %03d (ends at line %d): unmarshalling error %v", entities, lines, err)
			return entries, err
		}

		log.Debugf("entity %03d (ends at line %d): round %2d: unmarshalled entity %q", entities, lines, ge.Round, ge.Kind)

		switch ge.Kind {
		case KindPlayers:
			var ple playersEntry
			err = json.Unmarshal(entryRaw, &ple)
			if err != nil {
				return entries, err
			}
			entries[ple.Kind][ple.Round] = ple.Data
		case KindPairings:
			var pge pairingsEntry
			err = json.Unmarshal(entryRaw, &pge)
			if err != nil {
				return entries, err
			}
			entries[pge.Kind][pge.Round] = pge.Data
		case KindResults:
			var re resultsEntry
			err = json.Unmarshal(entryRaw, &re)
			if err != nil {
				return entries, err
			}
			entries[re.Kind][re.Round] = re.Data
		case KindLocation:
			var le locationEntry
			err = json.Unmarshal(entryRaw, &le)
			if err != nil {
				return entries, err
			}
			entries[le.Kind][le.Round] = le.Data
		}
		entities++
		buf.Reset()
	}

	return entries, nil
}

func (rj *RepoJLog) writeEntry(kind string, roundNum int, entity interface{}) error {
	var err error
	rj.log.Debugf("marshalling: %#v", entity)
	data, err := json.MarshalIndent(entity, "", "  ")
	if err != nil {
		return err
	}
	_, err = rj.sink.Write(data)
	if err != nil {
		return err
	}
	_, err = rj.sink.WriteString("\n" + separator + "\n")
	if err != nil {
		return err
	}
	err = rj.sink.Sync()
	if err != nil {
		return err
	}
	rj.entries[kind][roundNum] = entity
	return nil
}

func (rj *RepoJLog) SavePlayers(players []rdv1.Player) error {
	entry := playersEntry{
		Kind:  KindPlayers,
		Round: 0,
		Data:  players,
	}
	return rj.writeEntry(KindPlayers, 0, entry)
}

func (rj *RepoJLog) LoadPlayers() ([]rdv1.Player, error) {
	obj, ok := rj.entries[KindPlayers][0]
	if !ok {
		return nil, NotExist
	}
	ret, ok := obj.([]rdv1.Player)
	if !ok {
		return nil, InvalidObject
	}
	return ret, nil
}

func (rj *RepoJLog) LoadMatches(roundNum int) ([]rdv1.Match, error) {
	obj, ok := rj.entries[KindPairings][roundNum]
	if !ok {
		return nil, NotExist
	}
	ret, ok := obj.([]rdv1.Match)
	if !ok {
		return nil, InvalidObject
	}
	return ret, nil
}

func (rj *RepoJLog) SaveMatches(roundNum int, matches []rdv1.Match) error {
	entry := pairingsEntry{
		Kind:  KindPairings,
		Round: roundNum,
		Data:  matches,
	}
	return rj.writeEntry(KindPairings, roundNum, entry)
}

func (rj *RepoJLog) LoadMatchResults(roundNum int) ([]rdv1.MatchResult, error) {
	obj, ok := rj.entries[KindResults][roundNum]
	if !ok {
		return nil, NotExist
	}
	ret, ok := obj.([]rdv1.MatchResult)
	if !ok {
		return nil, InvalidObject
	}
	return ret, nil
}

func (rj *RepoJLog) SaveMatchResults(roundNum int, matchResults []rdv1.MatchResult) error {
	entry := resultsEntry{
		Kind:  KindResults,
		Round: roundNum,
		Data:  matchResults,
	}
	return rj.writeEntry(KindResults, roundNum, entry)
}

func (rj *RepoJLog) LoadLocation(roundNum int) (rdv1.Location, error) {
	obj, ok := rj.entries[KindLocation][roundNum]
	if !ok {
		return rdv1.Location{}, NotExist
	}
	ret, ok := obj.(rdv1.Location)
	if !ok {
		return rdv1.Location{}, InvalidObject
	}
	return ret, nil
}

func (rj *RepoJLog) SaveLocation(roundNum int, location rdv1.Location) error {
	entry := locationEntry{
		Kind:  KindLocation,
		Round: roundNum,
		Data:  location,
	}
	return rj.writeEntry(KindLocation, roundNum, entry)
}

func (rj *RepoJLog) Close() error {
	return rj.sink.Close()
}
