package datarepo

import (
	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

type RepoNull struct{}

func (rn RepoNull) CountRounds() (int, error) {
	return 0, nil
}

func (rn RepoNull) LoadPlayers() ([]rdv1.Player, error) {
	return nil, nil
}

func (rn RepoNull) LoadMatches(roundNum int) ([]rdv1.Match, error) {
	return nil, nil
}

func (rn RepoNull) LoadMatchResults(roundNum int) ([]rdv1.MatchResult, error) {
	return nil, nil
}

func (rn RepoNull) LoadLocation(roundnum int) (rdv1.Location, error) {
	return rdv1.Location{}, nil
}

func (rn RepoNull) SavePlayers(players []rdv1.Player) error {
	return nil
}

func (rn RepoNull) SaveMatches(roundNum int, matches []rdv1.Match) error {
	return nil
}

func (rn RepoNull) SaveMatchResults(roundNum int, matchResults []rdv1.MatchResult) error {
	return nil
}

func (rn RepoNull) SaveLocation(roundNum int, location rdv1.Location) error {
	return nil
}

func (rn RepoNull) Close() error {
	return nil
}
