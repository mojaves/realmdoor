package datarepo

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

type RepoDir struct {
	path string
}

func NewRepoDir(path string) RepoDir {
	return RepoDir{
		path: path,
	}
}

func (rd RepoDir) CountRounds() (int, error) {
	return 0, fmt.Errorf("not implemented")
}

func (rd RepoDir) SavePlayers(players []rdv1.Player) error {
	var err error
	fd, err := ioutil.TempFile(rd.path, "players-*.json")
	if err != nil {
		return err
	}
	defer fd.Close()
	err = json.NewEncoder(fd).Encode(players)
	if err != nil {
		os.Remove(fd.Name())
		return err
	}
	return os.Rename(fd.Name(), filepath.Join(rd.path, "players.json"))
}

func (rd RepoDir) LoadPlayers() ([]rdv1.Player, error) {
	fd, err := os.Open(filepath.Join(rd.path, "players.json"))
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil, NotExist
		}
		return nil, err
	}
	defer fd.Close()
	var players []rdv1.Player
	err = json.NewDecoder(fd).Decode(&players)
	return players, err
}

func (rd RepoDir) LoadMatches(roundNum int) ([]rdv1.Match, error) {
	fd, err := os.Open(filepath.Join(rd.path, fmt.Sprintf("matches-%02d.json", roundNum)))
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil, NotExist
		}
		return nil, err
	}
	defer fd.Close()
	var matches []rdv1.Match
	err = json.NewDecoder(fd).Decode(&matches)
	return matches, err
}

func (rd RepoDir) SaveMatches(roundNum int, matches []rdv1.Match) error {
	var err error
	fd, err := ioutil.TempFile(rd.path, fmt.Sprintf("matches-%02d-*.json", roundNum))
	if err != nil {
		return err
	}
	defer fd.Close()
	err = json.NewEncoder(fd).Encode(matches)
	if err != nil {
		os.Remove(fd.Name())
		return err
	}
	return os.Rename(fd.Name(), filepath.Join(rd.path, fmt.Sprintf("matches-%02d.json", roundNum)))
}

func (rd RepoDir) LoadMatchResults(roundNum int) ([]rdv1.MatchResult, error) {
	fd, err := os.Open(filepath.Join(rd.path, fmt.Sprintf("matchresults-%02d.json", roundNum)))
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil, NotExist
		}
		return nil, err
	}
	defer fd.Close()
	var matchResults []rdv1.MatchResult
	err = json.NewDecoder(fd).Decode(&matchResults)
	return matchResults, err

}

func (rd RepoDir) SaveMatchResults(roundNum int, matchResults []rdv1.MatchResult) error {
	var err error
	fd, err := ioutil.TempFile(rd.path, fmt.Sprintf("matchresults-%02d-*.json", roundNum))
	if err != nil {
		return err
	}
	defer fd.Close()
	err = json.NewEncoder(fd).Encode(matchResults)
	if err != nil {
		os.Remove(fd.Name())
		return err
	}
	return os.Rename(fd.Name(), filepath.Join(rd.path, fmt.Sprintf("matchresults-%02d.json", roundNum)))
}

func (rd RepoDir) LoadLocation(roundnum int) (rdv1.Location, error) {
	return rdv1.Location{}, nil
}

func (rd RepoDir) SaveLocation(roundNum int, location rdv1.Location) error {
	var err error
	fd, err := ioutil.TempFile(rd.path, fmt.Sprintf("matchlocation-%02d-*.json", roundNum))
	if err != nil {
		return err
	}
	defer fd.Close()
	err = json.NewEncoder(fd).Encode(location)
	if err != nil {
		os.Remove(fd.Name())
		return err
	}
	return os.Rename(fd.Name(), filepath.Join(rd.path, fmt.Sprintf("matchlocation-%02d.json", roundNum)))
}

func (rd RepoDir) Close() error {
	return nil
}
