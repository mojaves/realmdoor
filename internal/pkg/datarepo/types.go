package datarepo

import (
	"fmt"

	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

var (
	NotExist      = fmt.Errorf("entry not found")
	InvalidObject = fmt.Errorf("invalid object type")
)

type PlayerRepo interface {
	SavePlayers(players []rdv1.Player) error
	LoadPlayers() ([]rdv1.Player, error)
}

type MatchRepo interface {
	CountRounds() (int, error)
	SaveMatches(roundNum int, matches []rdv1.Match) error
	LoadMatches(roundNum int) ([]rdv1.Match, error)
	SaveMatchResults(roundNum int, matchResults []rdv1.MatchResult) error
	LoadMatchResults(roundNum int) ([]rdv1.MatchResult, error)
	SaveLocation(roundNum int, location rdv1.Location) error
	LoadLocation(roundnum int) (rdv1.Location, error)
}

// this must include ALL the Repo interfaces above
type DataRepo interface {
	Close() error
	PlayerRepo
	MatchRepo
}
