package datarepo

import "testing"

func TestNullLoad(t *testing.T) {
	rn := RepoNull{}

	rounds, err := rn.CountRounds()
	if err != nil {
		t.Fatalf("countrounds: unexpected error %v", err)
	}
	if rounds != 0 {
		t.Fatalf("countrounds: unexpected data %v", rounds)
	}

	players, err := rn.LoadPlayers()
	if err != nil {
		t.Fatalf("loadplayers: unexpected error %v", err)
	}
	if players != nil {
		t.Fatalf("loadplayers: unexpected data %v", players)
	}

	matches, err := rn.LoadMatches(0)
	if err != nil {
		t.Fatalf("loadmatches: unexpected error %v", err)
	}
	if matches != nil {
		t.Fatalf("loadmatches: unexpected data %v", players)
	}

	matchResults, err := rn.LoadMatchResults(0)
	if err != nil {
		t.Fatalf("loadmatchresults: unexpected error %v", err)
	}
	if matchResults != nil {
		t.Fatalf("loadmatchresults: unexpected data %v", players)
	}

}
