/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 Francesco Romani <fromani on gmail>
 */

package templateloader

import (
	"html/template"
	"io/ioutil"

	logger "github.com/apsdehal/go-logger"

	"gitlab.com/mojaves/realmdoor/generated/assets"
)

type TemplateLoader struct {
	log *logger.Logger
}

func New(log *logger.Logger) *TemplateLoader {
	return &TemplateLoader{
		log: log,
	}
}

func (tl *TemplateLoader) readTemplate(title, tmplPath string) ([]byte, error) {
	var data []byte
	var err error

	data, err = ioutil.ReadFile(tmplPath)
	if err == nil {
		tl.log.Debugf("loaded external template for '%s' (from '%s')", title, tmplPath)
		return data, err
	}
	tl.log.Debugf("error loading external template for '%s', fallback to default (%s)", title, err)

	data, err = assets.Asset(tmplPath)
	if err == nil {
		tl.log.Debugf("loaded embedded template for '%s' (from '%s')", title, tmplPath)
		return data, err
	}
	return nil, err
}

func (tl *TemplateLoader) LoadTemplate(title, tmplPath string) (*template.Template, error) {
	data, err := tl.readTemplate(title, tmplPath)
	if err != nil {
		return nil, err
	}

	t := template.New(title)
	t, err = t.Parse(string(data))
	if err != nil {
		tl.log.Warningf("error parsing template for '%s'", title)
		return nil, err
	}
	tl.log.Debugf("rendered template for '%s'", title)
	return t, err
}
