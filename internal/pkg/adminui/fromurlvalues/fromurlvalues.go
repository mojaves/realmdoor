/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 Francesco Romani <fromani on gmail>
 */

package fromurlvalues

import (
	"fmt"
	"net/url"
	"strconv"

	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

func GetPlayer(values url.Values) (rdv1.Player, error) {
	var player rdv1.Player
	var err error
	player.Firstname, err = MustGetRawValue(values, "firstname")
	if err != nil {
		return player, err
	}
	player.Lastname, err = MustGetRawValue(values, "lastname")
	if err != nil {
		return player, err
	}
	player.Faction, err = MustGetRawValue(values, "faction")
	if err != nil {
		return player, err
	}
	return player, nil
}

func GetMatchResult(values url.Values) (rdv1.MatchResult, error) {
	matchid, err := GetMatchID(values)
	if err != nil {
		return rdv1.MatchResult{}, err
	}

	defenderOutcome, attackerOutcome, err := ParseOutcomes(
		GetRawOutcome(values, 1),
		GetRawOutcome(values, 2),
	)
	if err != nil {
		return rdv1.MatchResult{}, err
	}

	defenderScore, err := GetScore(values, 1)
	if err != nil {
		return rdv1.MatchResult{}, err
	}

	attackerScore, err := GetScore(values, 2)
	if err != nil {
		return rdv1.MatchResult{}, err
	}

	return rdv1.MatchResult{
		MatchID: matchid,
		Defender: rdv1.MatchPlayerResultInfo{
			PlayerID: "",
			Outcome:  defenderOutcome,
			Score:    defenderScore,
		},
		Attacker: rdv1.MatchPlayerResultInfo{
			PlayerID: "",
			Outcome:  attackerOutcome,
			Score:    attackerScore,
		},
	}, nil
}

func GetMatchID(values url.Values) (rdv1.EID, error) {
	if matchidValue, ok := values["matchid"]; ok {
		if len(matchidValue) == 1 {
			return rdv1.EID(matchidValue[0]), nil
		}
	}
	return rdv1.EID(""), fmt.Errorf("malformed values looking for matchid")
}

func ParseOutcomes(defenderValue, attackerValue string) (rdv1.Outcome, rdv1.Outcome, error) {
	var ao rdv1.Outcome = rdv1.OutcomeDraw
	var do rdv1.Outcome = rdv1.OutcomeDraw
	var ok bool

	if defenderValue == "" && attackerValue == "" {
		return do, ao, fmt.Errorf("both outcomes not specifed")
	}

	if defenderValue != "" {
		defenderOutcome, err := strconv.Atoi(defenderValue)
		if err == nil {
			do = rdv1.Outcome(defenderOutcome)
			if attackerValue == "" {
				ao, ok = do.Opposite()
				if !ok {
					return do, ao, fmt.Errorf("invalid outcome")
				}
			}
		}
	}

	if attackerValue != "" {
		attackerOutcome, err := strconv.Atoi(attackerValue)
		if err == nil {
			ao = rdv1.Outcome(attackerOutcome)
			if defenderValue == "" {
				do, ok = ao.Opposite()
				if !ok {
					return do, ao, fmt.Errorf("invalid outcome")
				}
			}
		}
	}
	return do, ao, nil
}

func ParseInt(s string) (int, error) {
	if s == "" {
		return 0, nil
	}
	return strconv.Atoi(s)
}

func GetRawOutcome(values url.Values, outcomeIdx int) string {
	return getRawValueIdx(values, "outcome", outcomeIdx)
}

func GetScore(values url.Values, scoreIdx int) (rdv1.Score, error) {
	var err error
	var score rdv1.Score
	score.Primary, err = ParseInt(getRawValueIdx(values, "main", scoreIdx))
	if err != nil {
		return score, err
	}
	score.Secondary, err = ParseInt(getRawValueIdx(values, "secondary", scoreIdx))
	if err != nil {
		return score, err
	}
	score.Tertiary, err = ParseInt(getRawValueIdx(values, "extra", scoreIdx))
	if err != nil {
		return score, err
	}
	return score, err
}

func getRawValueIdx(values url.Values, prefix string, idx int) string {
	return GetRawValue(values, fmt.Sprintf("%s%d", prefix, idx))
}

func GetRawValue(values url.Values, name string) string {
	if rawValues, ok := values[name]; ok {
		if len(rawValues) > 0 {
			return rawValues[0]
		}
	}
	return ""
}

func MustGetRawValue(values url.Values, name string) (string, error) {
	if rawValues, ok := values[name]; ok {
		if len(rawValues) > 0 {
			return rawValues[0], nil
		}
	}
	return "", fmt.Errorf("no value for %q", name)
}
