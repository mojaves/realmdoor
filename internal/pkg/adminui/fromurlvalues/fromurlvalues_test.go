/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 Francesco Romani <fromani on gmail>
 */

package fromurlvalues_test

import (
	"net/url"
	"testing"

	"gitlab.com/mojaves/realmdoor/internal/pkg/adminui/fromurlvalues"
	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

func TestParseInt(t *testing.T) {
	var testCases = []struct {
		name          string
		s             string
		expectedValue int
		wantsError    bool
	}{
		{"empty", "", 0, false},
		{"zero", "0", 0, false},
		{"non-int", "XXX", 0, true},
		{"int", "42", 42, false},
	}
	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			got, err := fromurlvalues.ParseInt(tt.s)

			if err != nil {
				if !tt.wantsError {
					t.Errorf("unexpected error %v", err)
				}
			} else if got != tt.expectedValue {
				t.Errorf("expected %d got %d", tt.expectedValue, got)
			}
		})
	}
}

func TestGetRawOutcome(t *testing.T) {
	var testCases = []struct {
		name            string
		values          url.Values
		index           int
		expectedOutcome string
	}{
		{"empty", url.Values{}, 0, ""},
		{"nil values", url.Values{"outcome0": nil}, 0, ""},
		{"empty values", url.Values{"outcome0": []string{}}, 0, ""},
		{"mismatched values", url.Values{"outcome1": []string{}}, 2, ""},
		{"happy path", url.Values{"outcome1": []string{"2"}}, 1, "2"},
		{"extra data", url.Values{"outcome1": []string{"2", "2", "1"}}, 1, "2"},
	}
	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			got := fromurlvalues.GetRawOutcome(tt.values, tt.index)
			if got != tt.expectedOutcome {
				t.Errorf("expected %q got %q", tt.expectedOutcome, got)
			}
		})
	}
}

func TestGetMatchID(t *testing.T) {
	var testCases = []struct {
		name        string
		values      url.Values
		expectedEID rdv1.EID
		wantsError  bool
	}{
		{"empty", url.Values{}, rdv1.EID(""), true},
		{"happy path", url.Values{"matchid": []string{"foobar"}}, rdv1.EID("foobar"), false},
		{"extra data", url.Values{"outcome1": []string{"foo", "bar"}}, "", true},
	}
	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			got, err := fromurlvalues.GetMatchID(tt.values)
			if err != nil {
				if !tt.wantsError {
					t.Errorf("unexpected error %v", err)
				}
			} else if got != tt.expectedEID {
				t.Errorf("expected %q got %q", tt.expectedEID, got)
			}
		})
	}
}

func TestGetScore(t *testing.T) {
	var testCases = []struct {
		name          string
		values        url.Values
		index         int
		expectedScore rdv1.Score
		wantsError    bool
	}{
		{"empty", url.Values{}, 0, rdv1.Score{}, true},
		{"happy path",
			url.Values{
				"main0":      []string{"10"},
				"secondary0": []string{"20"},
				"extra0":     []string{"30"},
			},
			0,
			rdv1.Score{
				Primary:   10,
				Secondary: 20,
				Tertiary:  30,
			},
			false,
		},
		{"mismatched",
			url.Values{
				"main0":      []string{"10"},
				"secondary0": []string{"20"},
				"extra0":     []string{"30"},
			},
			1,
			rdv1.Score{},
			true,
		},
		{"malformed primary",
			url.Values{
				"main0":      []string{"10gg"},
				"secondary0": []string{"20"},
				"extra0":     []string{"30"},
			},
			0,
			rdv1.Score{},
			true,
		},
		{"malformed secondary",
			url.Values{
				"main0":      []string{"10"},
				"secondary0": []string{"20tt"},
				"extra0":     []string{"30"},
			},
			0,
			rdv1.Score{},
			true,
		},
		{"malformed tertiary",
			url.Values{
				"main0":      []string{"10"},
				"secondary0": []string{"20"},
				"extra0":     []string{"30zz"},
			},
			0,
			rdv1.Score{},
			true,
		},
		{"missing data",
			url.Values{
				"main0":      []string{},
				"secondary0": []string{},
				"extra0":     []string{},
			},
			0,
			rdv1.Score{},
			false,
		},
		{"missing primary",
			url.Values{
				"secondary0": []string{"20"},
				"extra0":     []string{"30"},
			},
			0,
			rdv1.Score{
				Secondary: 20,
				Tertiary:  30,
			},
			false,
		},
		{"missing secondary",
			url.Values{
				"main0":  []string{"10"},
				"extra0": []string{"30"},
			},
			0,
			rdv1.Score{
				Primary:  10,
				Tertiary: 30,
			},
			false,
		},
		{"missing tertiaary",
			url.Values{
				"main0":      []string{"10"},
				"secondary0": []string{"20"},
			},
			0,
			rdv1.Score{
				Primary:   10,
				Secondary: 20,
			},
			false,
		},
	}
	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			got, err := fromurlvalues.GetScore(tt.values, tt.index)
			if err != nil {
				if !tt.wantsError {
					t.Errorf("unexpected error %v", err)
				}
			} else if got != tt.expectedScore {
				t.Errorf("expected %v got %v", tt.expectedScore, got)
			}
		})
	}
}

func TestParseOutcomes(t *testing.T) {
	var testCases = []struct {
		name       string
		defValue   string
		atkValue   string
		defOutcome rdv1.Outcome
		atkOutcome rdv1.Outcome
		wantsError bool
	}{
		{"empty", "", "", rdv1.OutcomeDraw, rdv1.OutcomeDraw, true},
		{"both good", "2", "-2", rdv1.OutcomeMajorWin, rdv1.OutcomeMajorLoss, false},
		// yep, in case both are specified this function must NOT perform the validation
		{"both bad", "2", "-1", rdv1.OutcomeMajorWin, rdv1.OutcomeMinorLoss, false},
		{"miss def", "", "1", rdv1.OutcomeMinorLoss, rdv1.OutcomeMinorWin, false},
		{"miss atk", "2", "", rdv1.OutcomeMajorWin, rdv1.OutcomeMajorLoss, false},
		{"bad def", "4", "", rdv1.Outcome(4), rdv1.OutcomeDraw, true},
		{"bad atk", "", "-5", rdv1.OutcomeDraw, rdv1.Outcome(-5), true},
	}
	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			def, atk, err := fromurlvalues.ParseOutcomes(tt.defValue, tt.atkValue)
			if tt.wantsError && err == nil {
				t.Errorf("missing error, got %d/%d", def, atk)
			} else if !tt.wantsError && err != nil {
				t.Errorf("unexpected error %v", err)
			} else { // !wantsError && err == nil
				if def != tt.defOutcome || atk != tt.atkOutcome {
					t.Errorf("expected %v/%v got %v/%v", tt.defOutcome, tt.atkOutcome, def, atk)
				}
			}
		})
	}
}
