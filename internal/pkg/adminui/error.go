/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package adminui

import (
	"fmt"
	"html/template"
	"net/http"
)

const errorTemplate = `
<!DOCTYPE html>
<html>
	<head>
		<style>
			table {  border-collapse: collapse; width:  90%; }
			th { height: 50px; }
			table, th, td {  border: 1px solid black; }
			th, td { border-bottom: 1px solid #ddd; }
			tr:nth-child(even) {background-color: #f2f2f2;}
			.center { margin: auto; }
		</style>
		<meta charset="UTF-8">
		<title>{{ .Title }}</title>
	</head>
	<body>
		<div class="center">
		<p>
			{{ .Message }}
		</p>
		</div>
	</body>
</html>`

func (aui *AdminUI) errorPage(w http.ResponseWriter, extErr error) {
	tmpl, err := aui.loader.LoadTemplate("error", "assets/pages/error/index.html.tmpl")
	if err != nil {
		aui.log.Errorf("admin: error: error preparing page: %s", err)
		tmpl, _ = template.New("error").Parse(errorTemplate)
	}

	data := struct {
		Title   string
		Message string
	}{
		Title:   "Realmdoor internal error",
		Message: fmt.Sprintf("%v", extErr),
	}

	tmpl.Execute(w, data)
}
