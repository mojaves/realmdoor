/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package adminui

import (
	"net/http"

	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

type matchInfo struct {
	Table      int
	MatchID    string
	Player1    string
	Outcome1   string
	Main1      int
	Secondary1 int
	Extra1     int
	Player2    string
	Outcome2   string
	Main2      int
	Secondary2 int
	Extra2     int
}

func (aui *AdminUI) roundPage(w http.ResponseWriter, players []rdv1.Player, round *rdv1.Round) {
	matches, err := aui.cli.Matches()
	if err != nil {
		aui.log.Errorf("admin: round: error fetching matches: %s", err)
		aui.errorPage(w, err)
		return
	}

	matchResults, err := aui.cli.MatchResults()
	if err != nil {
		aui.log.Errorf("admin: round: error fetching match results: %s", err)
		aui.errorPage(w, err)
		return
	}

	playerMap := make(map[string]*rdv1.Player, len(players))
	for i, player := range players {
		playerMap[string(player.ID)] = &players[i]
	}
	resultsMap := make(map[string]*rdv1.MatchResult, len(matchResults))
	for i, result := range matchResults {
		resultsMap[string(result.MatchID)] = &matchResults[i]
	}

	matchInfos := make([]matchInfo, len(matches))
	for j, match := range matches {
		defender := playerMap[string(match.DefenderID)]
		attacker := playerMap[string(match.AttackerID)]
		result := resultsMap[string(match.ID)]

		mi := matchInfo{
			Table:   j + 1,
			MatchID: string(match.ID),
			Player1: defender.String(),
			Player2: attacker.String(),
		}
		if result != nil {
			mi.Outcome1 = result.Defender.Outcome.String()
			mi.Main1 = result.Defender.Score.Primary
			mi.Secondary1 = result.Defender.Score.Secondary
			mi.Extra1 = result.Defender.Score.Tertiary
			mi.Outcome2 = result.Attacker.Outcome.String()
			mi.Main2 = result.Attacker.Score.Primary
			mi.Secondary2 = result.Attacker.Score.Secondary
			mi.Extra2 = result.Attacker.Score.Tertiary
		} else {
			mi.Outcome1 = "pending..."
			mi.Outcome2 = "pending..."
		}
		matchInfos[j] = mi
	}

	tmpl, err := aui.loader.LoadTemplate("round status", "assets/pages/round/index.html.tmpl")
	if err != nil {
		aui.log.Errorf("admin: round: error preparing page: %s", err)
		aui.errorPage(w, err)
	}

	completed := false
	if round.CompletedMatches == round.TotalMatches {
		completed = true
	}

	data := struct {
		Round          int
		RoundCompleted bool
		Items          []matchInfo
	}{
		Items:          matchInfos,
		Round:          round.Number,
		RoundCompleted: completed,
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		aui.log.Errorf("admin: round: error rendering page: %s", err)
		aui.errorPage(w, err)
	}

}
