/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package adminui

import (
	"net/http"

	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

type playerPosition struct {
	Position       int
	Firstname      string
	Lastname       string
	Faction        string
	Tag            string
	PrimaryScore   int
	SecondaryScore int
	TertiaryScore  int
}

func (aui *AdminUI) leaderboardPage(w http.ResponseWriter, players []rdv1.Player, positions []rdv1.Position, round int) {
	tmpl, err := aui.loader.LoadTemplate("leaderboard", "assets/pages/leaderboard/index.html.tmpl")
	if err != nil {
		aui.log.Errorf("admin: leaderboard: error preparing page: %s", err)
		aui.errorPage(w, err)
	}

	playerMap := make(map[string]*rdv1.Player, len(players))
	for i, player := range players {
		playerMap[string(player.ID)] = &players[i]
	}

	var items []playerPosition
	for idx, pos := range positions {
		player := playerMap[string(pos.PlayerID)]
		items = append(items, playerPosition{
			Position:       idx + 1, // humans start counting from 1
			Firstname:      player.Firstname,
			Lastname:       player.Lastname,
			Faction:        player.Faction,
			Tag:            string(pos.PlayerID),
			PrimaryScore:   pos.Points.Primary,
			SecondaryScore: pos.Points.Secondary,
			TertiaryScore:  pos.Points.Tertiary,
		})
	}

	data := struct {
		Title string
		Items []playerPosition
		Round int
	}{
		Title: "Leaderboard",
		Items: items,
		Round: round,
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		aui.errorPage(w, err)
	}
}
