/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package adminui

import (
	"net/http"
	"net/url"

	logger "github.com/apsdehal/go-logger"

	"gitlab.com/mojaves/realmdoor/internal/pkg/adminui/fromurlvalues"
	"gitlab.com/mojaves/realmdoor/internal/pkg/templateloader"
	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
	admcli "gitlab.com/mojaves/realmdoor/pkg/client/v1/admin"
)

type AdminUI struct {
	log    *logger.Logger
	loader *templateloader.TemplateLoader
	cli    *admcli.Client
}

func New(log *logger.Logger, apiHost string, apiPort int) *AdminUI {
	return &AdminUI{
		log:    log,
		loader: templateloader.New(log),
		cli:    admcli.New(log, apiHost, apiPort),
	}
}

func (aui *AdminUI) adminIndex(w http.ResponseWriter, r *http.Request) {
	rd, err := aui.cli.CurrentRound()
	if err != nil {
		aui.log.Errorf("admin: index: error fetching round: %s", err)
		aui.errorPage(w, err)
		return
	}
	players, err := aui.cli.Players()
	if err != nil {
		aui.log.Errorf("admin: index: error fetching players: %s", err)
		aui.errorPage(w, err)
		return
	}

	if rd.Number == 0 {
		aui.playersPage(w, players, rd.Number)
		return
	}
	aui.roundPage(w, players, rd)
}

func (aui *AdminUI) adminPlayers(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		r.ParseForm()

		player, err := fromurlvalues.GetPlayer(r.Form)
		if err != nil {
			aui.log.Errorf("admin: players: error parsing players: %s", err)
			aui.errorPage(w, err)
			return
		}
		err = aui.cli.AddPlayers([]rdv1.Player{player})
		if err != nil {
			aui.log.Errorf("admin: players: error adding players: %s", err)
			aui.errorPage(w, err)
			return
		}
	}

	rd, err := aui.cli.CurrentRound()
	if err != nil {
		aui.log.Errorf("admin: players: error fetching round: %s", err)
		aui.errorPage(w, err)
		return
	}
	players, err := aui.cli.Players()
	if err != nil {
		aui.log.Errorf("admin: players: error fetching players: %s", err)
		aui.errorPage(w, err)
		return
	}

	aui.playersPage(w, players, rd.Number)
}

func (aui *AdminUI) handleMatchResultSubmit(rd *rdv1.Round, values url.Values) error {
	mr, err := fromurlvalues.GetMatchResult(values)
	if err != nil {
		aui.log.Errorf("admin: round: error parsing match result: %s", err)
		return err
	}

	err = aui.cli.AddMatchResults([]rdv1.MatchResult{mr})
	if err != nil {
		aui.log.Errorf("admin: round: error sending match result %v: %s", mr, err)
		return err
	}
	return nil
}

func (aui *AdminUI) handleRoundChange(rd *rdv1.Round) (*rdv1.Round, error) {
	var err error
	if rd.Number == 0 {
		err = aui.cli.FirstRound()
	} else {
		err = aui.cli.NextRound(rdv1.Location{})
	}
	if err != nil {
		aui.log.Errorf("admin: round: error switching round: %s", err)
		return nil, err
	}

	rd, err = aui.cli.CurrentRound()
	if err != nil {
		aui.log.Errorf("admin: round: error refreshing round: %s", err)
		return nil, err
	}
	return rd, nil
}

func (aui *AdminUI) adminRound(w http.ResponseWriter, r *http.Request) {
	var rd *rdv1.Round
	var err error

	rd, err = aui.cli.CurrentRound()
	if err != nil {
		aui.log.Errorf("admin: round: error fetching round: %s", err)
		aui.errorPage(w, err)
		return
	}

	if r.Method == http.MethodPost {
		var err error
		r.ParseForm()

		if _, ok := r.Form["matchid"]; ok {
			err = aui.handleMatchResultSubmit(rd, r.Form)
		} else {
			rd, err = aui.handleRoundChange(rd)
		}

		if err != nil {
			aui.errorPage(w, err)
			return
		}

		// need to refresh after match result submit
		rd, err = aui.cli.CurrentRound()
		if err != nil {
			aui.log.Errorf("admin: round: error re-fetching round: %s", err)
			aui.errorPage(w, err)
			return
		}
	}

	players, err := aui.cli.Players()
	if err != nil {
		aui.log.Errorf("admin: round: error fetching players: %s", err)
		aui.errorPage(w, err)
		return
	}

	aui.roundPage(w, players, rd)
}

func (aui *AdminUI) adminLeaderboard(w http.ResponseWriter, r *http.Request) {
	rd, err := aui.cli.CurrentRound()
	if err != nil {
		aui.log.Errorf("admin: leaderboard: error fetching round: %s", err)
		aui.errorPage(w, err)
		return
	}
	players, err := aui.cli.Players()
	if err != nil {
		aui.log.Errorf("admin: leaderboard: error fetching players: %s", err)
		aui.errorPage(w, err)
		return
	}

	positions, err := aui.cli.Leaderboard()
	if err != nil {
		aui.log.Errorf("admin: leaderboard: error fetching positions: %s", err)
		aui.errorPage(w, err)
		return
	}

	aui.leaderboardPage(w, players, positions, rd.Number)
}
