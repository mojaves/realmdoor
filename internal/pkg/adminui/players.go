/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package adminui

import (
	"net/http"

	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

func (aui *AdminUI) playersPage(w http.ResponseWriter, players []rdv1.Player, round int) {
	tmpl, err := aui.loader.LoadTemplate("players", "assets/pages/players/index.html.tmpl")
	if err != nil {
		aui.log.Errorf("admin: players: error preparing page: %s", err)
		aui.errorPage(w, err)
	}

	type playerItem struct {
		Row int
		rdv1.Player
	}

	var items []playerItem
	for row, player := range players {
		items = append(items, playerItem{
			Row:    row + 1,
			Player: player,
		})
	}

	data := struct {
		Title string
		Items []playerItem
		Round int
	}{
		Title: "Players",
		Items: items,
		Round: round,
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		aui.errorPage(w, err)
	}
}
