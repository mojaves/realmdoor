/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package adminui

import (
	"net/http"

	"github.com/gorilla/mux"
)

const (
	READ_CHUNK = 1048576
)

type Route struct {
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func (aui *AdminUI) Routes() Routes {
	var routes = Routes{
		Route{
			Method:      http.MethodGet,
			Pattern:     "/admin",
			HandlerFunc: aui.adminIndex,
		},
		Route{
			Method:      http.MethodGet,
			Pattern:     "/admin/players",
			HandlerFunc: aui.adminPlayers,
		},
		Route{
			Method:      http.MethodGet,
			Pattern:     "/admin/round",
			HandlerFunc: aui.adminRound,
		},
		Route{
			Method:      http.MethodGet,
			Pattern:     "/admin/leaderboard",
			HandlerFunc: aui.adminLeaderboard,
		},
		Route{
			Method:      http.MethodPost,
			Pattern:     "/admin/players",
			HandlerFunc: aui.adminPlayers,
		},
		Route{
			Method:      http.MethodPost,
			Pattern:     "/admin/round",
			HandlerFunc: aui.adminRound,
		},
	}
	return routes
}

func (aui *AdminUI) NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	routes := aui.Routes()
	for _, route := range routes {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Handler(route.HandlerFunc)
	}

	return router
}
