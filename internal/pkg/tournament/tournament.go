/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package tournament

import (
	"fmt"

	logger "github.com/apsdehal/go-logger"

	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

const (
	MinimumPlayers = 2
)

type Tournament struct {
	log *logger.Logger

	current     *Round
	previous    []*Round
	players     []rdv1.Player
	leaderboard rdv1.Leaderboard
}

func New(log *logger.Logger) *Tournament {
	return &Tournament{
		log: log,
	}
}

func (T *Tournament) Started() bool {
	return T.current != nil
}

func (T *Tournament) RoundCompleted() bool {
	if T.current == nil {
		return false
	}
	return T.current.IsCompleted()
}

func (T *Tournament) CurrentRoundNumber() (int, error) {
	if !T.Started() {
		return 0, fmt.Errorf("tournament hasn't started yet")
	}
	return len(T.previous) + 1, nil
}

func (T *Tournament) GetPositions() []rdv1.Position {
	pos := T.leaderboard.ToSlice()
	T.log.Debugf("positions: %#v", pos)
	return pos
}

func (T *Tournament) GetPlayers() []rdv1.Player {
	res := make([]rdv1.Player, len(T.players))
	copy(res, T.players)
	return res
}

func (T *Tournament) GetPlayerByID(playerID rdv1.EID) (rdv1.Player, bool) {
	for _, p := range T.players {
		if p.ID == playerID {
			return p, true
		}
	}
	return rdv1.Player{}, false
}

func (T *Tournament) GetPlayerByName(firstname, lastname string) (rdv1.Player, bool) {
	for _, p := range T.players {
		if p.Firstname == firstname && p.Lastname == lastname {
			return p, true
		}
	}
	return rdv1.Player{}, false
}

func (T *Tournament) GetPairings(roundNum int) ([]rdv1.Match, error) {
	R, err := T.getRound(roundNum)
	if err != nil {
		return nil, err
	}
	return R.GetPairings(), nil
}

func (T *Tournament) GetResults(roundNum int) ([]rdv1.MatchResult, error) {
	R, err := T.getRound(roundNum)
	if err != nil {
		return nil, err
	}
	return R.GetResults(), nil
}

func (T *Tournament) GetCurrentRound() (rdv1.Round, error) {
	if !T.Started() {
		return rdv1.Round{}, nil
	}
	roundNum, err := T.CurrentRoundNumber()
	if err != nil {
		return rdv1.Round{}, err
	}
	R, err := T.getRound(roundNum)
	if err != nil {
		return rdv1.Round{}, err
	}
	return R.ToAPI(roundNum), nil
}

// XXX
func (T *Tournament) Advance(toRoundNum int) (error, bool) {
	if !T.Started() {
		return fmt.Errorf("tournament hasn't started yet"), false
	}
	if !T.current.IsCompleted() {
		return nil, false
	}
	// TODO: validate/compute toRoundNum
	lb, err := updateLeaderboard(T.leaderboard, T.current, toRoundNum)
	if err != nil {
		return err, false
	}
	T.leaderboard = lb
	T.previous = append(T.previous, T.current)
	// XXX
	T.current, err = NewRoundSwiss(T.log, T, T.leaderboard, T.previous)
	return err, true
}

// XXX
func (T *Tournament) SetResult(mr rdv1.MatchResult) (error, bool) {
	if !T.Started() {
		return fmt.Errorf("tournament hasn't started yet"), false
	}
	err := T.current.SetResult(mr)
	return err, T.current.IsCompleted()
}

// XXX
func (T *Tournament) SetPlayerResult(mr rdv1.MatchPlayerResult) (error, bool) {
	if !T.Started() {
		return fmt.Errorf("tournament hasn't started yet"), false
	}
	return fmt.Errorf("not yet implemented"), false
}

// XXX
func (T *Tournament) Start() error {
	err := T.prestartCheck()
	if err != nil {
		return err
	}
	// XXX
	T.current, err = NewRoundRandom(T.log, T, T.players)
	return err
}

// XXX
func (T *Tournament) AddPlayers(players []rdv1.Player) error {
	if T.Started() {
		return fmt.Errorf("tournament already started")
	}
	var playerIDs []rdv1.EID
	for _, p := range players {
		var oldP rdv1.Player
		var ok bool

		if oldP, ok = T.GetPlayerByID(p.ID); ok {
			return fmt.Errorf("player %s already added", oldP)
		}
		if oldP, ok = T.GetPlayerByName(p.Firstname, p.Lastname); ok {
			return fmt.Errorf("player %s seems to be already added", oldP)
		}

		T.players = append(T.players, p)
		playerIDs = append(playerIDs, p.ID)
	}
	T.leaderboard = rdv1.NewLeaderboardForPlayers(playerIDs)
	return nil
}

func (T *Tournament) Restore(roundNum int, pairings []rdv1.Match, results []rdv1.MatchResult) error {
	var err error
	T.current, err = NewRoundFromMatches(T.log, T, roundNum, pairings)
	if err != nil {
		return err
	}
	for _, mr := range results {
		if err, _ := T.SetResult(mr); err != nil {
			return err
		}
	}
	lb, err := updateLeaderboard(T.leaderboard, T.current, roundNum)
	if err != nil {
		return err
	}
	T.leaderboard = lb
	return nil
}

func (T *Tournament) AdvanceRound() {
	completed := T.current.IsCompleted()
	if completed {
		T.previous = append(T.previous, T.current)
		T.current = nil
	}
	T.log.Debugf("previous rounds: %d", len(T.previous))
}

func (T *Tournament) prestartCheck() error {
	if T.Started() {
		return fmt.Errorf("tournament already started")
	}
	if len(T.players) <= MinimumPlayers {
		return fmt.Errorf("not enough players (%d)", len(T.players))
	}
	if len(T.players)%2 != 0 {
		return fmt.Errorf("odd number of players (%d)", len(T.players))
	}
	return nil
}

// roundNum == 0 means "current round"
func (T *Tournament) getRound(roundNum int) (*Round, error) {
	if !T.Started() {
		return nil, fmt.Errorf("tournment not started yet")
	}
	currentRound := len(T.previous) + 1
	var err error
	if roundNum == 0 {
		roundNum, err = T.CurrentRoundNumber()
		if err != nil {
			return nil, err
		}
	}
	if roundNum < 1 || roundNum > currentRound {
		return nil, fmt.Errorf("round number not in interval [1, %d]", currentRound)
	}
	if roundNum == currentRound {
		return T.current, nil
	}
	roundNum -= 1 // subtract 1 for T.current
	return T.previous[roundNum-1], nil
}

func updateLeaderboard(leaderboard rdv1.Leaderboard, R *Round, roundNum int) (rdv1.Leaderboard, error) {
	return leaderboard.Update(func(playerID rdv1.EID) (rdv1.Score, error) {
		ri, ok := R.GetResultInfoByPlayerID(playerID)
		if !ok {
			return ri.Score, fmt.Errorf("results not found in round %d for player %s", roundNum, playerID)
		}
		return ri.Score, nil

	})
}
