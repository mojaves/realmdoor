/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */
package tournament_test

import (
	"io/ioutil"
	"reflect"
	"testing"

	logger "github.com/apsdehal/go-logger"

	"gitlab.com/mojaves/realmdoor/internal/pkg/tournament"
	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
	"gitlab.com/mojaves/realmdoor/pkg/caosid"
)

func TestTournamentCreate(t *testing.T) {
	log, _ := logger.New(ioutil.Discard)
	tm := tournament.New(log)
	if tm.Started() {
		t.Errorf("tment unexpected started")
	}
	if tm.RoundCompleted() {
		t.Errorf("tment round unexpected completed")
	}
	num, err := tm.CurrentRoundNumber()
	if num != 0 || err == nil {
		t.Errorf("tment unexpected round number %d err %v", num, err)
	}
}

func TestTournamentStartWithoutPlayers(t *testing.T) {
	log, _ := logger.New(ioutil.Discard)
	tm := tournament.New(log)
	if tm.Started() {
		t.Errorf("tment unexpected started")
	}
	err := tm.Start()
	if err == nil {
		t.Errorf("tment started without players")
	}
	if tm.Started() {
		t.Errorf("tment unexpected started (post start attempt)")
	}
}

func TestTournamentAddPlayersDupID(t *testing.T) {
	eid := rdv1.EID(caosid.Long())
	players := newPlayers()
	players[1].ID = eid
	players[5].ID = eid

	log, _ := logger.New(ioutil.Discard)
	tm := tournament.New(log)
	err := tm.AddPlayers(players)
	if err == nil {
		t.Errorf("AddPlayers() succeeded, should fail!")
	}
}

func TestTournamentAddPlayersDupName(t *testing.T) {
	players := newPlayers()
	players[1].Firstname = "Doppio"
	players[1].Lastname = "Doppi"
	players[5].Firstname = "Doppio"
	players[5].Lastname = "Doppi"

	log, _ := logger.New(ioutil.Discard)
	tm := tournament.New(log)
	err := tm.AddPlayers(players)
	if err == nil {
		t.Errorf("AddPlayers() succeeded, should fail!")
	}
}

func TestTournamentAddPlayersOddNumber(t *testing.T) {
	players := newPlayers()

	log, _ := logger.New(ioutil.Discard)
	tm := tournament.New(log)
	err := tm.AddPlayers(players[1:])
	if err != nil {
		t.Errorf("AddPlayers() failed err %v", err)
	}

	err = tm.Start()
	if err == nil {
		t.Errorf("Start() succeeded!")
	}
}

func TestTournamentAddGetPlayers(t *testing.T) {
	players := newPlayers()

	log, _ := logger.New(ioutil.Discard)
	tm := tournament.New(log)
	err := tm.AddPlayers(players)
	if err != nil {
		t.Errorf("AddPlayers() failed err %v", err)
	}

	playersBack := tm.GetPlayers()
	if !reflect.DeepEqual(players, playersBack) {
		t.Errorf("GetPlayers() expected %v got %v", players, playersBack)
	}
}

func TestTournamentInitial(t *testing.T) {
	players := newPlayers()

	log, _ := logger.New(ioutil.Discard)
	tm := tournament.New(log)
	err := tm.AddPlayers(players)
	if err != nil {
		t.Errorf("AddPlayers() failed err %v", err)
	}

	err = tm.Start()
	if err != nil {
		t.Errorf("Start() failed err %v", err)
	}

	pairings, err := tm.GetPairings(0)
	if err != nil {
		t.Errorf("GetPairings() failed err %v", err)
	}
	if len(pairings) != len(players)/2 {
		t.Errorf("Unexpected pairings len: %d", len(pairings))
	}

	results, err := tm.GetResults(0)
	if err != nil {
		t.Errorf("GetResults() failed err %v", err)
	}
	if len(results) != 0 {
		t.Errorf("Unexpected results len: %d (expected 0)", len(results))
	}

	rnd, err := tm.GetCurrentRound()
	if err != nil {
		t.Errorf("GetCurrentRound() failed err %v", err)
	}

	if rnd.Number != 1 || rnd.CompletedMatches != 0 || rnd.TotalMatches != len(players)/2 {
		t.Errorf("Unexpected round data: %v", rnd)
	}
}
