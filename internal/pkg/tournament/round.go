/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package tournament

import (
	"fmt"
	"math/rand"

	logger "github.com/apsdehal/go-logger"

	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
	"gitlab.com/mojaves/realmdoor/pkg/caosid"
)

type Round struct {
	parent                *Tournament
	matches               []rdv1.Match
	matchResultsByMatchID map[rdv1.EID]rdv1.MatchResult
	matchIdxByPlayerID    map[rdv1.EID]int
	matchIdxByID          map[rdv1.EID]int
}

func (R *Round) IsCompleted() bool {
	if len(R.matchResultsByMatchID) == 0 {
		return false
	}
	for _, match := range R.matchResultsByMatchID {
		if !match.IsCompleted() {
			return false
		}
	}
	return true
}

func (R *Round) ToAPI(roundNum int) rdv1.Round {
	return rdv1.Round{
		Number:           roundNum,
		TotalMatches:     len(R.matches),
		CompletedMatches: len(R.matchResultsByMatchID),
	}
}

func (R *Round) GetPairings() []rdv1.Match {
	res := make([]rdv1.Match, len(R.matches))
	copy(res, R.matches)
	return res
}

func (R *Round) GetResults() []rdv1.MatchResult {
	res := make([]rdv1.MatchResult, 0, len(R.matches))
	for _, mr := range R.matchResultsByMatchID {
		res = append(res, mr)
	}
	return res
}

func (R *Round) OpponentOf(playerID rdv1.EID) (rdv1.EID, bool) {
	match, isDefender, found := R.matchByPlayerID(playerID)
	if !found {
		return rdv1.EID(""), false
	}
	if isDefender {
		return match.AttackerID, true
	}
	return match.DefenderID, true
}

func (R *Round) SetResult(mr rdv1.MatchResult) error {
	matchIdx, ok := R.matchIdxByID[mr.MatchID]
	if !ok {
		return fmt.Errorf("Unknown match for result %q (requested %q)", mr.ID, mr.MatchID)
	}
	match := R.matches[matchIdx]
	if err := mr.Validate(match); err != nil {
		return err
	}
	mr.Attacker.PlayerID = match.AttackerID
	mr.Defender.PlayerID = match.DefenderID
	R.matchResultsByMatchID[match.ID] = mr
	return nil
}

func (R *Round) SetPlayerResult(mpr rdv1.MatchPlayerResult) error {
	return fmt.Errorf("not implemented yet")
}

func (R *Round) GetResultInfoByPlayerID(playerID rdv1.EID) (rdv1.MatchPlayerResultInfo, bool) {
	match, isDefender, ok := R.matchByPlayerID(playerID)
	if !ok {
		return rdv1.MatchPlayerResultInfo{}, false
	}
	matchResult, ok := R.matchResultsByMatchID[match.ID]
	if !ok {
		return rdv1.MatchPlayerResultInfo{}, false
	}
	if isDefender {
		return matchResult.Defender, true
	}
	return matchResult.Attacker, true
}

func (R *Round) matchByPlayerID(playerID rdv1.EID) (rdv1.Match, bool, bool) {
	matchIdx, ok := R.matchIdxByPlayerID[playerID]
	if !ok {
		return rdv1.Match{}, false, false
	}
	match := R.matches[matchIdx]
	if playerID == match.DefenderID {
		return match, true, true
	}
	if playerID == match.AttackerID {
		return match, false, true
	}
	return rdv1.Match{}, false, false // how come?
}

func haveOpponentsMet(playerID, otherPlayerID rdv1.EID, previous []*Round) (bool, int, error) {
	for i, R := range previous {
		roundNum := len(previous) - i
		foundPlayerID, found := R.OpponentOf(playerID)
		if !found {
			return false, 0, fmt.Errorf("player %s not found in round %d matches", playerID, roundNum)
		}
		if foundPlayerID == otherPlayerID {
			return true, roundNum, nil
		}
	}
	return false, 0, nil
}

// if a pairing already happened in this tournament, we say two players "hold a grudge"
// (fantasy culture pun e.g. classic Dwarves intended, nothing else implied)
type grudge struct {
	PlayerAIdx int
	PlayerBIdx int
	RoundNum   int
}

func findGrudges(lb rdv1.Leaderboard, previous []*Round) (*grudge, error) {
	for i := 1; i < lb.Len(); i++ {
		met, roundNum, err := haveOpponentsMet(lb.PlayerAt(i-1), lb.PlayerAt(i), previous)
		if err != nil {
			return nil, err
		}
		if met {
			return &grudge{
				PlayerAIdx: i - 1,
				PlayerBIdx: i,
				RoundNum:   roundNum,
			}, nil
		}
	}
	return nil, nil
}
func NewRoundSwiss(log *logger.Logger, T *Tournament, leaderboard rdv1.Leaderboard, previous []*Round) (*Round, error) {
	res := leaderboard.Reversed() // now idx 0 -> lowest score
	tries := res.Len() - 2        // the last one cannot bubble up higher than second place!
	for t := 0; t < tries; t++ {
		log.Infof("swiss round, try %d", t)

		gd, err := findGrudges(res, previous)
		if err != nil {
			log.Warningf("error at try %d looking for grudges: %v", t, err)
			return nil, err
		}

		if gd == nil {
			break
		}
		log.Warningf("%q and %q already met in round %d, reshuffling and retrying", res.PlayerAt(gd.PlayerAIdx), res.PlayerAt(gd.PlayerBIdx), gd.RoundNum)

		// TODO: log
		if gd.PlayerBIdx == res.Len()-1 { // player B is the king of the hill
			res.Swap(gd.PlayerAIdx, gd.PlayerAIdx-1) // sift down
		} else {
			res.Swap(gd.PlayerBIdx, gd.PlayerBIdx+1) // bubble up
		}
	}
	gd, err := findGrudges(res, previous)
	if gd != nil {
		err = fmt.Errorf("%q and %q already met in round %d, can't find a valid combination", res.PlayerAt(gd.PlayerAIdx), res.PlayerAt(gd.PlayerBIdx), gd.RoundNum)
	}
	if err != nil {
		log.Warningf("failed to find pairings after %d tries: %v", tries, err)
		return nil, err
	}
	lb := res.Reversed()

	log.Debugf("swiss round created")
	newRoundNum := len(previous) + 1
	return NewRound(log, T, newRoundNum, lb.Players())
}

func NewRoundRandom(log *logger.Logger, T *Tournament, players []rdv1.Player) (*Round, error) {
	playerIDs := make([]rdv1.EID, len(players))
	for i := 0; i < len(players); i++ {
		playerIDs[i] = players[i].ID

	}
	newRoundNum := len(T.previous) + 1

	log.Noticef("round %d pairings begin with %d players", newRoundNum, len(playerIDs))

	rand.Shuffle(len(playerIDs), func(i, j int) { playerIDs[i], playerIDs[j] = playerIDs[j], playerIDs[i] })

	return NewRound(log, T, newRoundNum, playerIDs)
}

func NewRoundFromMatches(log *logger.Logger, T *Tournament, roundNum int, matches []rdv1.Match) (*Round, error) {
	// TODO: consistency with players?
	R := Round{
		parent:                T,
		matches:               make([]rdv1.Match, len(matches)),
		matchResultsByMatchID: make(map[rdv1.EID]rdv1.MatchResult),
		matchIdxByPlayerID:    make(map[rdv1.EID]int),
		matchIdxByID:          make(map[rdv1.EID]int),
	}

	for j, match := range matches {
		log.Infof("round %d pairing %02d: %#v", roundNum, j, match)
		R.matches[j] = match

		R.matchIdxByPlayerID[match.DefenderID] = j
		R.matchIdxByPlayerID[match.AttackerID] = j
		R.matchIdxByID[match.ID] = j
	}

	log.Noticef("round %d pairings done, ready", roundNum)
	return &R, nil
}

func NewRound(log *logger.Logger, T *Tournament, roundNum int, playerIDs []rdv1.EID) (*Round, error) {
	var matches []rdv1.Match
	for j := 0; j < len(playerIDs)/2; j++ {
		matches = append(matches, rdv1.Match{
			ID:         rdv1.EID(caosid.Short()),
			Round:      roundNum,
			DefenderID: playerIDs[j*2+0],
			AttackerID: playerIDs[j*2+1],
		})
	}
	return NewRoundFromMatches(log, T, roundNum, matches)
}
