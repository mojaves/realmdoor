/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */
package tournament_test

import (
	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
	"gitlab.com/mojaves/realmdoor/pkg/caosid"
)

func newPlayers() []rdv1.Player {
	return []rdv1.Player{
		rdv1.Player{
			ID:        rdv1.EID(caosid.Long()),
			Firstname: "Tizio",
			Lastname:  "Tiziani",
			Faction:   "Tizi rossi",
		},
		rdv1.Player{
			ID:        rdv1.EID(caosid.Long()),
			Firstname: "Caio",
			Lastname:  "Caiani",
			Faction:   "Cai verdi",
		},
		rdv1.Player{
			ID:        rdv1.EID(caosid.Long()),
			Firstname: "Sempronio",
			Lastname:  "Semproni",
			Faction:   "Semproni rossi",
		},
		rdv1.Player{
			ID:        rdv1.EID(caosid.Long()),
			Firstname: "Pippo",
			Lastname:  "Pippi",
			Faction:   "Pippi rossi",
		},
		rdv1.Player{
			ID:        rdv1.EID(caosid.Long()),
			Firstname: "Pinco",
			Lastname:  "Pinchi",
			Faction:   "Pinchi verdi",
		},
		rdv1.Player{
			ID:        rdv1.EID(caosid.Long()),
			Firstname: "Panco",
			Lastname:  "Panchi",
			Faction:   "Panchi verdi",
		},
		rdv1.Player{
			ID:        rdv1.EID(caosid.Long()),
			Firstname: "Tal",
			Lastname:  "Dei Tali",
			Faction:   "Tali verdi",
		},
		rdv1.Player{
			ID:        rdv1.EID(caosid.Long()),
			Firstname: "Nomen",
			Lastname:  "Nescio",
			Faction:   "Nemi blu",
		},
	}
}
