/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */
package tournament_test

import (
	"io/ioutil"
	"testing"

	logger "github.com/apsdehal/go-logger"

	"gitlab.com/mojaves/realmdoor/internal/pkg/tournament"
)

func TestRoundRandom(t *testing.T) {
	log, _ := logger.New(ioutil.Discard)
	tm := tournament.New(log)

	players := newPlayers()

	rnd, err := tournament.NewRoundRandom(log, tm, players)
	if err != nil {
		t.Errorf("NewRoundRandom unexpectedly fail err %v", err)
	}

	pairings := rnd.GetPairings()
	if len(pairings) != len(players)/2 {
		t.Errorf("Unexpected pairings len: %d", len(pairings))
	}

	if rnd.IsCompleted() {
		t.Errorf("empty round reported completed")
	}
}
