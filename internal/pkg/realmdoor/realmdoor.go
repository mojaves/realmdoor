/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package realmdoor

import (
	"encoding/json"
	"net/http"
	"strconv"

	logger "github.com/apsdehal/go-logger"
	"github.com/gorilla/mux"

	"gitlab.com/mojaves/realmdoor/internal/pkg/datarepo"
	"gitlab.com/mojaves/realmdoor/internal/pkg/tournament"
	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

type Realmdoor struct {
	info  rdv1.RealmInfo
	log   *logger.Logger
	repo  datarepo.DataRepo
	tment *tournament.Tournament
}

func New(log *logger.Logger, repo datarepo.DataRepo, info rdv1.RealmInfo) *Realmdoor {
	return &Realmdoor{
		info:  info,
		log:   log,
		repo:  repo,
		tment: tournament.New(log),
	}
}

func (rd *Realmdoor) getRound(w http.ResponseWriter, r *http.Request) {
	rr, err := rd.tment.GetCurrentRound()
	if err != nil {
		rd.log.Warningf("error getting current round: %v", err)
		invalidDataRequestError(w, err)
		return
	}

	rd.log.Debugf("sending round: %#v", rr)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err := json.NewEncoder(w).Encode(rr); err != nil {
		outputError(w, err)
		return
	}
}

func (rd *Realmdoor) getRealmInfo(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err := json.NewEncoder(w).Encode(rd.info); err != nil {
		outputError(w, err)
		return
	}
}

func (rd *Realmdoor) getLeaderboard(w http.ResponseWriter, r *http.Request) {
	positions := rd.tment.GetPositions()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err := json.NewEncoder(w).Encode(positions); err != nil {
		outputError(w, err)
		return
	}
}

func (rd *Realmdoor) getPlayerAll(w http.ResponseWriter, r *http.Request) {
	players := rd.tment.GetPlayers()
	rd.sendPlayers(players, w)
}

func (rd *Realmdoor) getPairingsByCurrentRound(w http.ResponseWriter, r *http.Request) {
	pairings, err := rd.tment.GetPairings(0)
	if err != nil {
		rd.log.Warningf("error getting pairings: %v", err)
		invalidDataRequestError(w, err)
		return
	}
	rd.sendPairings(pairings, w)
}

func (rd *Realmdoor) getPairingByRoundNum(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	roundNum, err := strconv.Atoi(vars["roundNum"])
	if err != nil {
		rd.log.Warningf("error getting the round number: %v", err)
		invalidDataRequestError(w, err)
		return
	}

	pairings, err := rd.tment.GetPairings(roundNum)
	if err != nil {
		rd.log.Warningf("error getting pairings: %v", err)
		invalidDataRequestError(w, err)
		return
	}
	rd.sendPairings(pairings, w)
}
func (rd *Realmdoor) getResultsByCurrentRound(w http.ResponseWriter, r *http.Request) {
	results, err := rd.tment.GetResults(0)
	if err != nil {
		rd.log.Warningf("error getting results: %v", err)
		invalidDataRequestError(w, err)
		return
	}
	rd.sendResults(results, w)
}

func (rd *Realmdoor) getResultByRoundNum(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	roundNum, err := strconv.Atoi(vars["roundNum"])
	if err != nil {
		rd.log.Warningf("error getting the round number: %v", err)
		invalidDataRequestError(w, err)
		return
	}

	results, err := rd.tment.GetResults(roundNum)
	if err != nil {
		rd.log.Warningf("error getting results: %v", err)
		invalidDataRequestError(w, err)
		return
	}
	rd.sendResults(results, w)
}
func (rd *Realmdoor) getResultByPlayerID(w http.ResponseWriter, r *http.Request) {
	gerr := ErrorGeneric{
		Code:    ErrorUnimplemented,
		Message: "Not implemented yet",
	}
	JSONError(w, gerr, http.StatusInternalServerError)
	return
}

func (rd *Realmdoor) sendResults(results []rdv1.MatchResult, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err := json.NewEncoder(w).Encode(results); err != nil {
		outputError(w, err)
		return
	}
}

func (rd *Realmdoor) sendPairings(pairings []rdv1.Match, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err := json.NewEncoder(w).Encode(pairings); err != nil {
		outputError(w, err)
		return
	}
}

func (rd *Realmdoor) sendPlayers(players []rdv1.Player, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err := json.NewEncoder(w).Encode(players); err != nil {
		outputError(w, err)
		return
	}
}
