/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package realmdoor

import (
	"net/http"

	"github.com/gorilla/mux"
)

const (
	READ_CHUNK = 1048576
)

type Route struct {
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func (rd *Realmdoor) Routes() Routes {
	var routes = Routes{
		Route{
			Method:      http.MethodGet,
			Pattern:     "/api/v1/realminfo",
			HandlerFunc: rd.getRealmInfo,
		},
		Route{
			Method:      http.MethodGet,
			Pattern:     "/api/v1/leaderboard",
			HandlerFunc: rd.getLeaderboard,
		},
		Route{
			Method:      http.MethodGet,
			Pattern:     "/api/v1/round",
			HandlerFunc: rd.getRound,
		},
		Route{
			Method:      http.MethodGet,
			Pattern:     "/api/v1/player",
			HandlerFunc: rd.getPlayerAll,
		},
		Route{
			Method:      http.MethodGet,
			Pattern:     "/api/v1/pairing/round",
			HandlerFunc: rd.getPairingsByCurrentRound,
		},
		Route{
			Method:      http.MethodGet,
			Pattern:     "/api/v1/pairing/round/{roundNum}",
			HandlerFunc: rd.getPairingByRoundNum,
		},
		Route{
			Method:      http.MethodGet,
			Pattern:     "/api/v1/result/round",
			HandlerFunc: rd.getResultsByCurrentRound,
		},
		Route{
			Method:      http.MethodGet,
			Pattern:     "/api/v1/result/round/{roundNum}",
			HandlerFunc: rd.getResultByRoundNum,
		},
		Route{
			Method:      http.MethodGet,
			Pattern:     "/api/v1/result/player/{playerID}",
			HandlerFunc: rd.getResultByPlayerID,
		},
		Route{
			Method:      http.MethodPost,
			Pattern:     "/api/v1/player",
			HandlerFunc: rd.addPlayer,
		},
		Route{
			Method:      http.MethodPost,
			Pattern:     "/api/v1/result/match",
			HandlerFunc: rd.addResultForMatch,
		},
		Route{
			Method:      http.MethodPost,
			Pattern:     "/api/v1/result/player",
			HandlerFunc: rd.addResultForPlayer,
		},
		Route{
			Method:      http.MethodPost,
			Pattern:     "/api/v1/round/next",
			HandlerFunc: rd.nextRound,
		},
		Route{
			Method:      http.MethodPost,
			Pattern:     "/api/v1/round/first",
			HandlerFunc: rd.firstRound,
		},
	}
	return routes
}

func (rd *Realmdoor) NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	routes := rd.Routes()
	for _, route := range routes {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Handler(route.HandlerFunc)
	}

	return router
}
