/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package realmdoor

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
	"gitlab.com/mojaves/realmdoor/pkg/caosid"
)

func (rd *Realmdoor) setPlayersID(players []rdv1.Player) error {
	for i := 0; i < len(players); i++ {
		players[i].ID = rdv1.EID(caosid.Short())
		rd.log.Infof("player %s %s given ID %s", players[i].Firstname, players[i].Lastname, players[i].ID)
	}
	return nil
}

func (rd *Realmdoor) addPlayer(w http.ResponseWriter, r *http.Request) {
	var players []rdv1.Player
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, READ_CHUNK))
	if err != nil {
		inputError(w, err)
		return
	}
	if err := r.Body.Close(); err != nil {
		inputError(w, err)
		return
	}

	if err := json.Unmarshal(body, &players); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		if err := json.NewEncoder(w).Encode(err); err != nil {
			outputError(w, err)
			return
		}
	}

	rd.setPlayersID(players)

	err = rd.tment.AddPlayers(players)
	if err != nil {
		invalidStatusError(w, err)
		return
	}
}

func (rd *Realmdoor) addResultForPlayer(w http.ResponseWriter, r *http.Request) {
	gerr := ErrorGeneric{
		Code:    ErrorUnimplemented,
		Message: "Not implemented yet",
	}
	JSONError(w, gerr, http.StatusInternalServerError)
	return
}

func (rd *Realmdoor) addResultForMatch(w http.ResponseWriter, r *http.Request) {
	var results []rdv1.MatchResult
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, READ_CHUNK))
	if err != nil {
		inputError(w, err)
		return
	}
	if err := r.Body.Close(); err != nil {
		inputError(w, err)
		return
	}

	if err := json.Unmarshal(body, &results); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		if err := json.NewEncoder(w).Encode(err); err != nil {
			outputError(w, err)
			return
		}
	}

	resultProcessed := 0
	resultCount := len(results)
	rd.log.Debugf("processing %03d results...", resultCount)

	for j, result := range results {
		result.ID = rdv1.EID(caosid.Short())

		err, completed := rd.tment.SetResult(result)
		if completed && (j < (resultCount - 1)) {
			rd.log.Warningf("round completed too early")
			invalidStatusError(w, fmt.Errorf("round completed too early, unprocessed results"))
			return
		}

		if err != nil {
			rd.log.Warningf("round processing error: %#v", err)
			invalidStatusError(w, err)
			return
		}

		rd.log.Debugf("processed result %03d/%03d: %v", j, resultCount, result)
		resultProcessed++
	}
	rd.log.Debugf("processed %03d results, checkpointing...", resultCount)

	hasCheckpoint := false
	roundNum, err := rd.tment.CurrentRoundNumber()
	if err == nil {
		hasCheckpoint = rd.checkpointRoundMatchResults(roundNum)
	}

	type ResultResponse struct {
		ResultPersisted bool `json:"result_persisted"`
		ResultProcessed int  `json:"result_processed"`
		RoundCompleted  bool `json:"round_completed"`
	}
	rr := ResultResponse{
		ResultPersisted: hasCheckpoint,
		ResultProcessed: resultProcessed,
		RoundCompleted:  rd.tment.RoundCompleted(),
	}
	rd.log.Debugf("completed: %#v", rr)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err := json.NewEncoder(w).Encode(rr); err != nil {
		outputError(w, err)
		return
	}
}

func (rd *Realmdoor) nextRound(w http.ResponseWriter, r *http.Request) {
	rd.log.Infof("advancing to next round")
	roundNum, err := rd.tment.CurrentRoundNumber()
	if err != nil {
		invalidStatusError(w, err)
		return
	}
	if !rd.tment.RoundCompleted() {
		invalidStatusError(w, fmt.Errorf("round incomplete"))
		return
	}

	loc := rdv1.Location{}
	done := rd.checkpointRound(roundNum, loc)
	if !done {
		rd.log.Warningf("Error saving location for round %d: %v - continuing", roundNum, err)
	}

	err, ok := rd.tment.Advance(roundNum + 1)
	if err != nil {
		invalidStatusError(w, err)
		return
	}
	if !ok {
		invalidStatusError(w, fmt.Errorf("cannot (yet) advance from round %d", roundNum))
		return
	}

	roundNum, err = rd.tment.CurrentRoundNumber()
	if err == nil {
		rd.checkpointRoundMatchPairings(roundNum)
	} else {
		// intentionally ignore errors, just log
		rd.log.Errorf("Error getting the current round: %v", err)
	}
}

func (rd *Realmdoor) makeFirstRound() error {
	rd.log.Infof("calling first round")
	err := rd.tment.Start()
	if err != nil {
		rd.log.Warningf("failed to start tournament: %v", err)
		return err
	}

	pairings, err := rd.tment.GetPairings(1)
	if err != nil {
		rd.log.Warningf("failed to get pairings for round 1: %v", err)
		return err
	}

	if err := rd.repo.SavePlayers(rd.tment.GetPlayers()); err != nil {
		rd.log.Warningf("failed to save players (%s), continuing", err)
	}
	if err := rd.repo.SaveMatches(1, pairings); err != nil {
		rd.log.Warningf("failed to save matches (%s), continuing", err)
	}
	return err
}

func (rd *Realmdoor) firstRound(w http.ResponseWriter, r *http.Request) {
	err := rd.makeFirstRound()
	if err != nil {
		invalidStatusError(w, err)
		return
	}
}
