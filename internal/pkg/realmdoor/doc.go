/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package realmdoor

import (
	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
)

type RouteParam struct {
	Method  string
	Pattern string
	Input   interface{}
	Output  interface{}
}

var params = []RouteParam{
	RouteParam{
		Method:  "GET",
		Pattern: "/api/v1/leaderboard",
		Output:  []rdv1.Position{rdv1.Position{}},
	},
	RouteParam{
		Method:  "GET",
		Pattern: "/api/v1/round",
		Output:  rdv1.Round{},
	},
	RouteParam{
		Method:  "GET",
		Pattern: "/api/v1/player",
		Output:  []rdv1.Player{rdv1.Player{}},
	},
	RouteParam{
		Method:  "GET",
		Pattern: "/api/v1/player/{playerID}",
		Output:  []rdv1.Player{rdv1.Player{}},
	},
	RouteParam{
		Method:  "GET",
		Pattern: "/api/v1/pairing/round",
		Output:  []rdv1.Match{rdv1.Match{}},
	},
	RouteParam{
		Method:  "GET",
		Pattern: "/api/v1/pairing/round/{roundNum}",
		Output:  []rdv1.Match{rdv1.Match{}},
	},
	RouteParam{
		Method:  "GET",
		Pattern: "/api/v1/result/round",
		Output:  []rdv1.MatchResult{rdv1.MatchResult{}},
	},
	RouteParam{
		Method:  "GET",
		Pattern: "/api/v1/result/round/{roundNum}",
		Output:  []rdv1.MatchResult{rdv1.MatchResult{}},
	},
	RouteParam{
		Method:  "GET",
		Pattern: "/api/v1/result/{matchId}",
		Output:  []rdv1.MatchResult{rdv1.MatchResult{}},
	},
	RouteParam{
		Method:  "POST",
		Pattern: "/api/v1/player",
		Input:   []rdv1.Player{rdv1.Player{}},
	},
	RouteParam{
		Method:  "POST",
		Pattern: "/api/v1/result/match",
		Input:   []rdv1.MatchResult{rdv1.MatchResult{}},
	},
	RouteParam{
		Method:  "POST",
		Pattern: "/api/v1/result/player",
		Input:   []rdv1.MatchPlayerResult{rdv1.MatchPlayerResult{}},
	},
	RouteParam{
		Method:  "POST",
		Pattern: "/api/v1/round/next",
	},
	RouteParam{
		Method:  "POST",
		Pattern: "/api/v1/round/first",
	},
}

func (rd *Realmdoor) RouteParamsByPattern(method, pattern string) (RouteParam, bool) {
	for _, rp := range params {
		if method == rp.Method && pattern == rp.Pattern {
			return rp, true
		}
	}
	return RouteParam{}, false
}

func (rd *Realmdoor) RouteParams() []RouteParam {
	return params
}
