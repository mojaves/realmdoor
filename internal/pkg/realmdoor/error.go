/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package realmdoor

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const (
	ErrorNone = iota
	ErrorUnknown
	ErrorUnimplemented
	ErrorInternal
	ErrorDecodingInput
	ErrorEncodingOutput
	ErrorInvalidRound
	ErrorInvalidDataRequest
	ErrorInvalidStatus
)

type ErrorGeneric struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func JSONError(w http.ResponseWriter, err interface{}, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(err)
}

func outputError(w http.ResponseWriter, err error) {
	gerr := ErrorGeneric{
		Code:    ErrorEncodingOutput,
		Message: fmt.Sprintf("%s", err),
	}
	JSONError(w, gerr, http.StatusInternalServerError)
}

func inputError(w http.ResponseWriter, err error) {
	gerr := ErrorGeneric{
		Code:    ErrorDecodingInput,
		Message: fmt.Sprintf("%s", err),
	}
	JSONError(w, gerr, http.StatusUnprocessableEntity)
}

func invalidDataRequestError(w http.ResponseWriter, err error) {
	gerr := ErrorGeneric{
		Code:    ErrorInvalidDataRequest,
		Message: fmt.Sprintf("%s", err),
	}
	JSONError(w, gerr, http.StatusUnprocessableEntity)
}

func invalidStatusError(w http.ResponseWriter, err error) {
	gerr := ErrorGeneric{
		Code:    ErrorInvalidStatus,
		Message: fmt.Sprintf("%s", err),
	}
	JSONError(w, gerr, http.StatusUnprocessableEntity)
}
