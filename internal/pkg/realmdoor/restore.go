/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020-2021 Francesco Romani <fromani on gmail>
 */

package realmdoor

import (
	"gitlab.com/mojaves/realmdoor/internal/pkg/datarepo"

	rdv1 "gitlab.com/mojaves/realmdoor/pkg/api/v1"
	"gitlab.com/mojaves/realmdoor/pkg/caosid"
)

func (rd *Realmdoor) checkpointRound(roundNum int, loc rdv1.Location) bool {
	loc.ID = rdv1.EID(caosid.Long())

	// we want to save it anyway -even if invalid- to have a consistent file set.
	err := rd.repo.SaveLocation(roundNum, loc)
	if err != nil {
		rd.log.Warningf("failed to save round %d localization (%s), continuing", roundNum, err)
		return false
	}

	if !loc.IsValid() {
		rd.log.Warningf("invalid location, not updating matches for round %d", roundNum)
		return false
	}

	done := true
	pairings, err := rd.tment.GetPairings(roundNum)
	if err != nil {
		rd.log.Warningf("failed to get round %d matches (%s), continuing", roundNum, err)
		done = false
	} else {
		for idx := 0; idx < len(pairings); idx++ {
			pairings[idx].LocationID = loc.ID
			pairings[idx].Round = roundNum
		}
		err := rd.repo.SaveMatches(roundNum, pairings)
		if err != nil {
			rd.log.Warningf("failed to save round %d pairings (%s), continuing", roundNum, err)
			done = false
		}
	}

	return done

}

func (rd *Realmdoor) checkpointRoundMatchPairings(roundNum int) bool {
	done := true

	pairings, err := rd.tment.GetPairings(roundNum)
	if err != nil {
		rd.log.Warningf("failed to get round %d matches (%s), continuing", roundNum, err)
		done = false
	} else {
		err := rd.repo.SaveMatches(roundNum, pairings)
		if err != nil {
			rd.log.Warningf("failed to save round %d pairings (%s), continuing", roundNum, err)
			done = false
		}
	}

	return done
}

func (rd *Realmdoor) checkpointRoundMatchResults(roundNum int) bool {
	done := true

	results, err := rd.tment.GetResults(roundNum)
	if err != nil {
		rd.log.Warningf("failed to get round %d results (%s), continuing", roundNum, err)
		done = false
	} else {
		err := rd.repo.SaveMatchResults(roundNum, results)
		if err != nil {
			rd.log.Warningf("failed to save round %d results (%s), continuing", roundNum, err)
			done = false
		}
	}

	return done
}

func (rd *Realmdoor) Replay() error {
	players, err := rd.repo.LoadPlayers()
	if err != nil {
		rd.log.Warningf("error loading players: %v - continuing", err)
		return err
	}
	// TODO: what about tampering?
	if err := rd.tment.AddPlayers(players); err != nil {
		rd.log.Warningf("error re-adding players: %v - continuing", err)
		return err
	}

	rd.log.Infof("players restored: %v", rd.tment.GetPositions())

	roundNum := 1
	recovered := 0
	for {
		matches, err := rd.repo.LoadMatches(roundNum)
		// TODO: break if filenotfound
		if err != nil {
			if err == datarepo.NotExist {
				rd.log.Infof("no matches file found for round %d", roundNum)
				break
			} else {
				rd.log.Warningf("cannot restore matches for round %d: %v", roundNum, err)
				return err
			}
		}
		results, err := rd.repo.LoadMatchResults(roundNum)
		if err != nil {
			if err == datarepo.NotExist {
				rd.log.Infof("no results file found for round %d", roundNum)
				results = []rdv1.MatchResult{}
			} else {
				rd.log.Warningf("cannot restore results for round %d: %v", roundNum, err)
				return err
			}
		}

		if roundNum > 1 {
			rd.tment.AdvanceRound()
		}

		err = rd.tment.Restore(roundNum, matches, results)
		if err != nil {
			rd.log.Warningf("cannot restore round %d: %v", roundNum, err)
			return err
		}
		if !rd.tment.RoundCompleted() {
			break
		}

		rd.log.Infof("leaderboard restored after round %d: %v", roundNum, rd.tment.GetPositions())
		roundNum++
		recovered++
	}

	if recovered == 0 {
		rd.makeFirstRound()
	}
	rd.log.Infof("leaderboard restored final: %v", rd.tment.GetPositions())
	rnd, err := rd.tment.GetCurrentRound()
	if err != nil {
		return err
	}
	rd.log.Infof("current round is: %#v", rnd)
	return nil
}
