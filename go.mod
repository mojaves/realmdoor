module gitlab.com/mojaves/realmdoor

go 1.15

require (
	github.com/apsdehal/go-logger v0.0.0-20190515212710-b0d6ccfee0e6
	github.com/gorilla/mux v1.7.3
	github.com/spf13/pflag v1.0.5
)
