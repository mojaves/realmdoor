# Realmdoor roadmap

## preview version (somewhere in 0.0.z)
At this point you can tinker around with the webui, and run it in a tournament,
but still you can't trust it yet as main source of thruth as TO.

Highlights:
- core logic implemented and with functests
- working admin webui
- ...

## alpha version (somewhere in 0.10.z)
At this point you are supposed to be able to run a tournament with realmdoor, as TO.

Highlights:
- webui decent looking and complete.
- core logic throughtly tested (functests, unit tests)


## beta version (somewhere in 0.20.z)
At this point you can start enjoying the real benefits of realmdoor as TO, letting
user upload their results and easily check the pairings

Highlights:
- player webui (aka account management)
- admin/player split
- admin webui allows to amend players results

## GA version (somewhere in 1.0.z)
Everything works nicely, all is good

Highlights
- focus on UI/UX
- bell & whistles
